<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">

                    {{--// profile image and display name of user--}}
                    <span>
                            <img alt="image" class="img-circle" style="    max-width: 7rem;"
                                 src="{{auth()->user()->photo}}">
                             </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear">
                                <span class="block m-t-xs">
                                    <strong class="font-bold">{{Auth::user()->name}}</strong>
                                    {{--@foreach(auth()->user()->roles as $role)--}}
                                    {{--<span class="label label-success">{{$role->display_name}}</span>--}}
                                    {{--@endforeach--}}
                                </span>
                            </span>
                    </a>


                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        {{--<!-- <li><a href="">حسابي</a></li>--}}
                        {{--<li class="divider"></li> -->--}}

                        <li>

                            <a href="{{url('reset-password')}}"> <i class="fa fa-key" style="margin-left: 5px;"></i>
                                تغيير كلمة المرور </a>

                        </li>
                        <li>
                            <script type="">
                                function submitSidebarSignout() {
                                    document.getElementById('signoutSidebar').submit();

                                }
                            </script>
                            {!! Form::open(['method' => 'post', 'url' => url('logout'),'id'=>'signoutForm','id'=>'signoutSidebar']) !!}
                            {!! Form::hidden('guard','admin') !!}

                            {!! Form::close() !!}
                            <a href="#" onclick="submitSidebarSignout()"> <i class="fa fa-sign-out"
                                                                             style="margin-left: 5px;"></i> تسجيل الخروج
                            </a>

                        </li>
                    </ul>
                </div>

                <div class="logo-element">

                    <img src="{{auth()->user()->photo}}" style="   max-width: 5rem; margin-top: 20px; margin-bottom:auto;"
                         alt="logo">
                </div>
            </li>
            {{--Home--}}
            <li>
                <a href="{{url('home')}}">
                    <i class="fa fa-home"></i>
                    <span class="nav-label">الرئيسية</span>
                </a>
            </li>

            <li>
                <a href="{{url('users')}}">
                    <i class="fa fa-users" aria-hidden="true"></i>
                    <span class="nav-label"> المستخدمين</span>
                </a>
            </li>


            <li>
                <a href="{{url('roles')}}">
                    <i class="fa fa-user-circle" aria-hidden="true"></i>
                    <span class="nav-label">رتب المستخدمين</span>
                </a>
            </li>

            <li>
                <a href="{{url('folders')}}">
                    <i class="fa fa-folder" aria-hidden="true"></i>
                    <span class="nav-label">المجلدات</span>
                </a>
            </li>

            <li>
                <a href="{{url('emails')}}">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <span class="nav-label">رسائل البريد الإلكتروني</span>
                </a>
            </li>

            <li>
                <a href="{{url('logs')}}">
                    <i class="fa fa-paste" aria-hidden="true"></i>
                    <span class="nav-label">سجلات العمليات </span>
                </a>
            </li>

        </ul>

    </div>
</nav>