@extends('layouts.main',[
                                    'page_header'       => 'الصفحة الرئيسية',
                                    'page_description'  => '',
                                    'link' => url('home')
                                ])

@section('content')
    @push('styles')
        {{-- ChartStyle --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
    @endpush

{{--    @push('scripts')--}}
{{--        {!! $pies_folders->script() !!}--}}
{{--        {!! $pies_files->script() !!}--}}
{{--    @endpush--}}
    @inject('contents' , 'App\Models\Content')
    @inject('logs' , 'App\Models\Log')
    @inject('attachments' , 'App\Models\Attachment')
    @inject('files' , 'App\Models\File')
    @inject('folders' , 'App\Models\Folder')
    @inject('roles' , 'Spatie\Permission\Models\Role')
    @inject('permissions' , 'Spatie\Permission\Models\Permission')
    @inject('users' , 'App\Models\User')
    @inject('notifications' , 'App\Models\Notification')

    <br>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">

{{--            <div class="col-md-5">--}}
{{--                <div class="ibox ">--}}
{{--                    <div class="ibox-title">--}}
{{--                        <h5>رسم بياني للمجلدات</h5>--}}
{{--                    </div>--}}
{{--                    <div class="ibox-content">--}}
{{--                        {!!$pies_folders->container() !!}--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <div class="col-md-5">--}}
{{--                <div class="ibox ">--}}
{{--                    <div class="ibox-title">--}}
{{--                        <h5>رسم بياني للملفات</h5>--}}
{{--                    </div>--}}
{{--                    <div class="ibox-content">--}}
{{--                        {!!$pies_files->container() !!}--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
            <div class="clearfix"></div>
            <a href="{{url('folders')}}" class="col-lg-4">
                <div class="widget style1 navy-bg">
                    <div class="row">
                        <div class="col-lg-4">
                            <i class="fa fa-folder-open fa-5x"></i>
                        </div>
                        <div class="col-8 text-right">
                            <span> المجلدات </span>
                            <h2 class="font-bold">{{$folders->count()}}</h2>
                        </div>
                    </div>
                </div>
            </a>
            <a class="col-lg-4">
                <div class="widget style1 yellow-bg">
                    <div class="row">
                        <div class="col-lg-4">
                            <i class="fa fa-file fa-5x"></i>
                        </div>
                        <div class="col-8 text-right">
                            <span> الملفات </span>
                            <h2 class="font-bold">{{$files->count()}}</h2>
                        </div>
                    </div>
                </div>
            </a>
            <a class="col-lg-4">
                <div class="widget style1 lazur-bg">
                    <div class="row">

                        <div class="col-lg-4" style="text-align: right">
                            <i class="fa fa-paperclip fa-5x"></i>
                        </div>
                        <div class="col-8 text-right">
                            <span> المقالات</span>
                            <h2 class="font-bold">{{$contents->count()}}</h2>
                        </div>

                    </div>
                </div>
            </a>
        </div>
        <br>
        <div class="row">
            <a href="{{url('users')}}" class="col-lg-4">
                <div class="widget style1 navy-bg">
                    <div class="row vertical-align">
                        <div class="col-4">
                            <i class="fa fa fa-users fa-3x"></i>
                        </div>
                        <div class="col-9 text-right">
                            <h2 class="font-bold">{{$users->count() }}</h2>
                        </div>
                    </div>
                </div>
            </a>
            <a href="{{url('notifications')}}" class="col-lg-4">
                <div class="widget style1 navy-bg">
                    <div class="row vertical-align">
                        <div class="col-4">
                            <i class="fa fa-bell fa-3x"></i>
                        </div>
                        <div class="col-9 text-right">
                            <h2 class="font-bold">{{$notifications->count()}}</h2>
                        </div>
                    </div>
                </div>
            </a>
            <a href="{{url('logs')}}" class="col-lg-4">
                <div class="widget style1 lazur-bg">
                    <div class="row vertical-align">
                        <div class="col-4">
                            <i class="fa fa-paste fa-3x"></i>
                        </div>
                        <div class="col-9 text-right">
                            <h2 class="font-bold">{{$logs->count()}}</h2>
                        </div>
                    </div>
                </div>
            </a>

        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-lg-12">
                <table class="table">
                    <tbody>
                    <tr>
                        <td>
                            <a class="btn btn-default m-r-sm">{{$attachments->count()}}</a>
                            المرفقات
                        </td>
                        <td>
                            <a href="{{url('roles')}}" class="btn btn-success m-r-sm">{{$roles->count()}}</a>
                            الصلاحيات
                        </td>
                        <td>
                            <a href="{{url('users')}}" class="btn btn-info m-r-sm">{{$users->count()}}</a>
                            المديرين
                        </td>

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


    </div>

@endsection