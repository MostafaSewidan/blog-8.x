<div class="row border-bottom">

    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary pull-right" href="#"><i
                        class="fa fa-bars"></i> </a>

        </div>

        <ul class="nav navbar-top-links navbar-left">


            <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" aria-expanded="false">
                    <i class="fa fa-bell" style="color: #3c8cbc;font-weight: bolder;font-size: 1.9rem;"></i>
                    <span
                            class="label label-danger"
                            id="pin_code_number">
                            {{auth()->user()->new_notifi_count}}
                        </span>

                </a>
                <ul class="dropdown-menu dropdown-messages" id="pin_code_row"
                    style="overflow: auto;    max-height: 40rem; width: 35rem;">

                    @forelse(auth()->user()->notifications()->get() as $notifi)
                        <li>
                            <a
                                    href="{{url('file/'.optional(\App\Models\Content::find($notifi->notifiable_id))->file_id.'/contents?id='.$notifi->notifiable_id)}}"
                                    onclick="readNotification('{{url('notifications/read/'.$notifi->id)}}')"
                                    @if($notifi->pivot->is_read == 0)  style="background-color: #337ab7b8 ;color: white" @endif
                            >
                                <div class="dropdown-messages-box">
                                    <div class="media-body">
                                        <small class="pull-left">
                                              منذ {{$notifi->created_at->diffForHumans(Carbon\Carbon::now())}}</small>
                                            <strong>{{$notifi->title}}</strong>

                                    </div>
                                </div>
                            </a>
                        </li>

                        @if($loop->iteration == 5)
                            @break;
                        @endif
                        <hr>
                    @empty

                    @endforelse

                    <li class="divider"></li>
                    <li>
                        <div class="text-center link-block">
                            <a href="{{url('notifications')}}">
                                <strong>عرض الكل</strong>
                            </a>
                        </div>
                    </li>
                </ul>
            </li>


            <li>
                <script type="">
                    function submitSignout() {
                        document.getElementById('signoutForm').submit();

                    }
                </script>
                {!! Form::open(['method' => 'post', 'url' => url('logout'),'id'=>'signoutForm']) !!}
                {!! Form::hidden('guard','admin') !!}
                {!! Form::close() !!}

                <a href="#" onclick="submitSignout()">
                    <i class="fa fa-sign-out"></i> تسجيل الخروج
                </a>
            </li>


        </ul>

    </nav>
</div>