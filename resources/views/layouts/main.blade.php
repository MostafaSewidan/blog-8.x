@include('layouts.head')

<div id="wrapper">

    @include('layouts.sidebar')

    <div id="page-wrapper" class="gray-bg">

        @include('layouts.header')


        <div class="row wrapper border-bottom white-bg page-heading" style="min-height: 0; margin-top: 25px;">
            <div class="col-lg-9">
                <h2>
                    <a href="{{$link}}">

                        {{$page_header}}
                    </a>
                    <small>{!! $page_description !!}</small>
                </h2>
                @if(!empty($links))
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{url('/')}}">الرئيسية</a>
                        </li>
                        @foreach($links as $key => $val)
                            <li class="breadcrumb-item {{$loop->last ? 'active':''}}">
                                <a href="{{$val}}">{!! $loop->last ? '<strong>'.$key.'</strong>' : $key !!}</a>
                            </li>
                        @endforeach
                    </ol>

                @endif
            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    @yield('content')
                </div>
            </div>
        </div>

        @include('layouts.footer')
    </div>

</div>
@include('layouts.foot')