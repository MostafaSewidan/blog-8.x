@component('mail::message')
<h3>{{$message->title}}</h3>{!! $message->content !!}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
