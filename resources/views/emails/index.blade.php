@extends('layouts.main',[
								'page_header'		=> 'رسائل البريد الإلكتروني',
								'page_description'	=> 'عرض ',
								'link' => url('message')
								])
@section('content')
    <div class="ibox box-primary">
        <div class="ibox-title">
            <div class="pull-right">
                <a href="{{url('emails/create')}}" class="btn btn-primary">
                    <i class="fa fa-envelope"></i> إرسال رسالة جديدة
                </a>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="row">
            {!! Form::open([
                'method' => 'GET'
            ]) !!}
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    {!! Form::text('name',old('name'),[
                        'class' => 'form-control',
                        'placeholder' => 'الاسم'
                    ]) !!}
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    {!! Form::text('from',old('from'),[
                        'class' => 'form-control datepicker',
                        'placeholder' => 'بداية تاريخ الاضافة'
                    ]) !!}
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    {!! Form::text('to',old('to'),[
                        'class' => 'form-control datepicker',
                        'placeholder' => 'انتهاء تاريخ الاضافة'
                    ]) !!}
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    <button class="btn btn-flat btn-block btn-primary">بحث</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

        <div class="ibox-content">
            @if(!empty($records) && count($records)>0)
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <th>#</th>
                        <th class="text-center">العنوان</th>
                        <th class="text-center">التفاصيل</th>
                        <th class="text-center">المرسل</th>
                        <th class="text-center">إعادة إرسال</th>
                        <th class="text-center">حذف</th>
                        </thead>
                        <tbody>

                        @foreach($records as $record)
                            <tr id="removable{{$record->id}}">
                                <td>{{$loop->iteration}}</td>
                                <td class="text-center">
                                    {{$record->title}}
                                </td>
                                <td class="text-center">

                                    <button style="background-color: white;border: none ;"
                                            data-toggle="modal" data-target="#email{{$record->id}}">
                                        <i class="btn btn-xs btn-info fa fa-eye" style="padding: 4px 6px"></i>
                                    </button>
                                </td>
                                <!-- Modal -->
                                <div class="modal fade" id="email{{$record->id}}" tabindex="-1" role="dialog"
                                     aria-labelledby="myModalLabel">

                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close"><span aria-hidden="true">&times;</span>
                                                </button>


                                                <h2 style="font-weight: bold" class="modal-title"
                                                    id="myModalLabel"> عرض محتوي الرساله رقم : {{$record->id}}</h2>
                                            </div>
                                            <br>
                                            <div class="modal-body">
                                                <p><strong>العنوان
                                                        : </strong> {{$record->title}}
                                                </p>
                                                <p><strong>المحتوي
                                                        : </strong>
                                                </p>
                                                {!! $record->content !!}
                                            </div>
                                            <div class="modal-footer">
                                                <p><strong>تاريخ الاضافة
                                                        : </strong> {{$record->created_at->locale('ar')->isoFormat('dddd  , MMMM  ,  Do / YYYY  ,  الساعة h:mm')}}
                                                </p>
                                                <button type="button" class="btn btn-default" data-dismiss="modal"
                                                        style="background-color: #00c0ef;color: white">
                                                    إغلاق
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <td class="text-center">
                                    <a href="{{url('users?id='.optional($record->sender)->id)}}">{{optional($record->sender)->name}}</a>
                                </td>
                                <td class="text-center">
                                    <a href="{{url('emails/' . $record->id .'/edit')}}" class="btn btn-xs btn-success"><i class="fa fa-reply"></i></a></td>

                                <td class="text-center">
                                    <button
                                            id="{{$record->id}}"
                                            data-token="{{ csrf_token() }}"
                                            data-route="{{URL::route('emails.destroy',$record->id)}}"
                                            type="button" class="destroy btn btn-danger btn-xs"><i class="fa fa-trash-o"></i></button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {!! $records->render() !!}
            @else
                <div>
                    <h3 class="text-info" style="text-align: center"> لا توجد بيانات للعرض </h3>
                </div>
            @endif


        </div>
    </div>
@stop

