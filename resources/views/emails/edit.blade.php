
@extends('layouts.main',[
                                'page_header'       => 'رسائل البريد الإلكتروني',
                                'page_description'  => ' إعادة ارسال  ',
                                'link' => url('roles')
                                ])
@section('content')
        <!-- general form elements -->
<div class="box box-primary">
    <!-- form start -->
    {!! Form::model($model,[
                            'url'=>url('emails/'.$model->id),
                            'id'=>'ajaxForm',
                            'role'=>'form',
                            'method'=>'PUT',
                            'files' => true
                            ])!!}

    <div class="box-body">
        <div class="clearfix"></div>
        <br>
        @include('emails.form')

        <div class="box-footer">
            {!! \Helper\Field::ajaxBtn('إرسال') !!}
        </div>

    </div>
    {!! Form::close()!!}

</div><!-- /.box -->

@endsection


















