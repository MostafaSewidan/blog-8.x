@extends('layouts.main',[
                                'page_header'       => 'رسائل البريد الإلكتروني',
                                'page_description'  => '  إرسال جديد ',
                                'link' => url('message')
                                ])
@section('content')
        <!-- general form elements -->
<div class="box box-primary">
    <!-- form start -->
    {!! Form::model($model,[
                            'action'=>'Admin\EmailController@store',
                            'id'=>'ajaxForm',
                            'role'=>'form',
                            'method'=>'POST',
                            'files' => true
                            ])!!}

    <div class="box-body">

        @include('emails.form')

        <div class="box-footer">
            {!! \Helper\Field::ajaxBtn('إرسال') !!}
        </div>

    </div>
    {!! Form::close()!!}

</div><!-- /.box -->

@endsection