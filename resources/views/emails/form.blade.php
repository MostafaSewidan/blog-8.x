@inject('users','App\Models\User')

{!! \Helper\Field::text('title' , 'العنوان' )!!}
{!! \Helper\Field::editor('content' , 'المحتوي' ) !!}
{!! \Helper\Field::multiSelect('users' , 'إلي', $users->pluck('name','id')->toArray(), request('to',null)) !!}
