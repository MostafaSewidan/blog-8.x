@extends('layouts.main',[
								'page_header'		=> 'عرض المجلدات',
								'page_description'	=> 'عرض ',
								'link' => url('folders/'),
								'links' => [
								    'المجلدات' => url('folders/')
								],
								])
@section('content')


    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-content">
                        <div class="file-manager">

                            <a href="{{url('folders?show=block-show')}}" class="btn btn-info m-r-sm">
                                <i class="fa fa-th-large"></i>
                            </a>
                            <a href="{{url('folders?show=layer-show')}}" class="btn btn-info m-r-sm">
                                <i class="fa fa-th-list"></i>
                            </a>
                            <div class="hr-line-dashed"></div>
                            @can('add_folder')

                                <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#add-new"><i
                                            class="fa fa-plus"></i> اضافة جديد
                                </button>
                                <div class="hr-line-dashed"></div>

                                <div class="modal fade" id="add-new" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">
                                                    إضافة جديد
                                                </h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            {!! Form::open([
                                                                                                         'action'=>'Admin\FolderController@store',
                                                                                                         'id'=>'ajaxForm',
                                                                                                         'role'=>'form',
                                                                                                         'method'=>'POST',
                                                                                                         'files' => true
                                                                                                         ])!!}

                                            <div class=" modal-body">


                                                <div class="box-body">

                                                    {!! \Helper\Field::select('status' , 'الحالة', \App\Models\Folder::$status) !!}
                                                    {!! \Helper\Field::text('name' , 'إسم المجلد') !!}
                                                </div>
                                            </div>
                                            <div class="modal-footer">

                                                <div class="box-footer">
                                                    {!! \Helper\Field::ajaxBtn('حفظ') !!}
                                                    <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">غلق
                                                    </button>
                                                </div>
                                            </div>

                                            {!! Form::close()!!}
                                        </div>
                                    </div>
                                </div>
                            @endcan

                            @can('edit_folder')
                                <div class="modal fade" id="edit-model" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel"
                                     aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">
                                                    تعديل
                                                </h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            {!! Form::open([
                                                 'id'=>'editForm',
                                                 'role'=>'form',
                                                'method'=>'PUT',
                                                 'files' => true
                                                 ])!!}

                                            <div class=" modal-body">


                                                <div class="box-body">

                                                    {!! \Helper\Field::select('edit_status' , 'الحالة', \App\Models\Folder::$status) !!}
                                                    {!! \Helper\Field::text('edit_name' , 'إسم المجلد') !!}
                                                </div>
                                            </div>
                                            <div class="modal-footer">

                                                <div class="box-footer">
                                                    <button type="submit" class="btn btn-primary">حفظ</button>
                                                    <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">غلق
                                                    </button>
                                                </div>
                                            </div>

                                            {!! Form::close()!!}
                                        </div>
                                    </div>
                                </div>
                            @endcan

                            <h3>البحث</h3>

                            {!! Form::open([
                                   'method' => 'get'
                                   ]) !!}
                            {!! Form::hidden('show' , request('show','block-show')) !!}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::text('name',request()->input('name'),[
                                            'class' => 'form-control',
                                            'placeholder' => 'إسم المجلد'
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::select('folder_search_status',\App\Models\Folder::$status,request('folder_search_status'),[
                                                                        'class' => 'form-control',
                                                                        'placeholder' => 'كل المجلدات'
                                                                    ]) !!}
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::text('from',request()->input('from'),[
                                            'class' => 'form-control datepicker',
                                            'autocomplete' => 'off',
                                            'placeholder' => 'من'
                                        ]) !!}
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        {!! Form::text('to',\Request::input('to'),[
                                            'class' => 'form-control datepicker',
                                            'autocomplete' => 'off',
                                            'placeholder' => 'إلى'
                                        ]) !!}
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button class="btn btn-flat btn-block btn-primary">بحث</button>
                                    </div>
                                </div>

                            </div>

                            {!! Form::close() !!}
                            <div class="hr-line-dashed"></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">

                        @if(!empty($records) && count($records)>0)
                            @include('folders.'.
                            (in_array(request('show' , 'block-show'),['block-show','layer-show'])
                                                    ? request('show' , 'block-show') : 'block-show'))


                            {!! $records->render() !!}
                        @else
                            <div>
                                <h3 class="text-info" style="text-align: center"> لا توجد بيانات للعرض </h3>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@can('edit_folder')
    <script>
        function openEditModel(model) {
            $('#editForm').attr('action', '{{url('folders')}}/' + model.id);

            $('#edit_name').val(model.name);
            $('#edit_status').val(model.status);
            $('#edit_status').select2().trigger('change');
            $('#edit-model').modal('show');
        }

    </script>
@endcan

