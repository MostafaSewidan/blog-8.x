<div class="attachment">
    @php $count = 1; @endphp
    @foreach($records as $record)

        <div class="file-box" id="removable{{$record->id}}">
            <div class="file">
                <div class="ibox-tools">

                    @if($record->user_id == auth()->user()->id)
                        @can('edit_folder')
                            <a onclick="openEditModel({{$record}})"><i
                                        class="fa fa-edit"></i></a>
                        @endcan

                        @can('delete_folder')
                            <button
                                    id="{{$record->id}}"
                                    data-token="{{ csrf_token() }}"
                                    data-route="{{URL::route('folders.destroy',$record->id)}}"
                                    type="button" class="destroy"
                                    style="color: #c7c7c7;background-color: #ffffff00;border: none;"
                            ><i class="fa fa-times"></i>
                            </button>
                        @endcan
                    @endif
                </div>
                <a href="{{url('folder/'.$record->id.'/files')}}">
                    <span class="corner"></span>

                    <div class="icon">
                        <i class="fa fa-folder-open" style="color: #ff980099;"></i>
                    </div>
                    <div class="file-name">
                        <span style="font-size: 1.6rem;">{{$record->name}}</span>
                        <span style="font-size: 1.6rem;float: left;">
                                            @if($record->status == 'private')
                                <i class="fa fa-lock"></i>
                            @else
                                <i class="fa fa-users"></i>
                            @endif
                                        </span>
                        <br>
                        <br>

                        <i class="fa fa-clock-o"></i>
                        <small>{{$record->created_at->locale('ar')->diffForHumans(Carbon\Carbon::now())}}</small>
                    </div>
                </a>
            </div>

        </div>

        @php $count ++; @endphp
    @endforeach
</div>