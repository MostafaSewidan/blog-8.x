
{!! \Helper\Field::text('name' , 'الاسم')!!}
{!! \Helper\Field::email('email' , 'البريد الالكترونى') !!}
{!! \Helper\Field::password('password' , 'كلمة المرور') !!}
{!! \Helper\Field::password('password_confirmation' , 'تاكيد كلمة المرور') !!}
<br>
<div class="form-group" id="permissions_wrap">
    <label for="permissions">الرتب</label>
    <div class="">
        <br>

        @foreach( $roles as $role)

            <div class="form-group col-lg-4" id="roles_wrap">

                <div class="">
                    <input type="checkbox" name="roles[]" value="{{$role->id}}"
                            {{$model->hasRole($role->name) ? 'checked' : ''}}>

                    <label for="roles">{{$role->display_name}}</label>
                </div>
            </div>

        @endforeach
    </div>
    <br>
    <br>

</div>


