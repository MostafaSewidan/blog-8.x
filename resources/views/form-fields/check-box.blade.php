
<div class="form-group {{$errors->has($name) ? 'has-error':'' }}" id="{{$name}}_wrap">
    <label for="{{$name}}">{{$label}}</label>
    <div class="row">

        <br>
        <div class="form-group col-lg-12">

            <div class="">
                <span style="    color: white;
    background-color: #3c8cbc;
    padding: 9px 9px;
    border-radius: 6px;">
                    <input type="checkbox" onClick="toggle(this , '{{$name}}')" name="sellectAll">

                    <label for="sellectAll">تحديد الكل</label>
                </span>
            </div>
        </div>
        @foreach($options as $key => $val)
            <div class="col-lg-2">
                <input type="checkbox" name="{{$name}}[]" value="{{$key}}" {{$value && in_array($key , $value) ? 'checked' : ''}}>
                <label for="permissions">{{$val}}</label>
            </div>
        @endforeach
    </div>

    <span class="help-block"><strong id="{{$name}}_error">{{$errors->first($name)}}</strong></span>
</div>