<div class="row" style="margin: 3%;
    background-color: #2f40500f;
    padding: 10px 24px;">

    <div class="form-group {{$errors->has($name) ? 'has-error':'' }}" id="{{$name}}_wrap">
        <label for="{{$name}}">{{$label}}</label>
        <div class="clearfix"></div>
        <div class="col-md-6">
            <div class="">
                {!! Form::text($name.'input', null, [
                "placeholder" => $label,
                "class" => "form-control",
                "id" => $name
                ]) !!}
            </div>
        </div>

        <div class="col-md-6">
            <button type="button" class="btn btn-success" id="multi-add-btn" onclick="multiAddVal('{{$name}}')"><i
                        class="fa fa-plus"></i></button>
        </div>

        <div class="clearfix"></div>
        <span class="help-block"><strong id="{{$name}}_error">{{$errors->first($name)}}</strong></span>
    </div>
    <div class="clearfix"></div>

    <div style="margin-top: 15px;" id="{{$name}}_inputs_content">

        @if($value && !empty($value))
            @foreach($value as $val)
                <span>
                    <span class="label label-primary" style="margin: 0px 8px;">
                        <span style="margin-right: 6px;cursor: pointer;font-weight: bolder;"
                              onclick="multiRemoveVal(this)">x</span>
                        <span>{{$val}}</span>
                        <input type="hidden" name="points[]" value="{{$val}}">
                    </span>
                    <br><br>
                </span>
            @endforeach
        @endif
    </div>
</div>

<script>
    function multiAddVal(input_id) {
        var $input = $('#' + input_id);
        var $inputs_content = $('#{{$name}}_inputs_content');

        if ($input.val() === '') {
            $input.focus();

        } else {

            $inputs_content.append('<span><span class="label label-primary" style="margin: 0px 8px;">' +
                '<span style="margin-right: 6px;cursor: pointer;font-weight: bolder;" onclick="multiRemoveVal(this)">x</span>' +
                '<span>' + $input.val() + '</span>' +
                '<input type="hidden" name="{{$name}}[]" value="' + $input.val() + '"></span><br><br></span>');
            $input.val('');
        }
    }

    function multiRemoveVal(tag) {
        $(tag).parent().parent().remove();
    }
</script>