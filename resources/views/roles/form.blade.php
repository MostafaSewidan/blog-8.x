{!! \Helper\Field::text('name' , 'الاسم الصلاحية' )!!}
{!! \Helper\Field::text('display_name' , 'الاسم المعروض' ) !!}
<br>
<div class="form-group" id="permissions_wrap">
    <label for="permissions">الصلاحيات</label>
    <div class="">
        <br>

        <div class="form-group col-lg-12">

            <div class="">
                <span style="    color: white;
    background-color: #3c8cbc;
    padding: 9px 9px;
    border-radius: 6px;">
                    <input type="checkbox" onClick="toggle(this , 'permissions')" name="sellectAll">

                    <label for="sellectAll">تحديد الكل</label>
                </span>
            </div>
        </div>

        <div class="clearfix"></div>

        @php
            $title = '';
        @endphp
        @foreach( $permissions as $permission)
            @if ($loop->first)
                @php
                    $title = $permission->category;
                @endphp

                <div class="clearfix"></div>
                <br>
                <br>
                <div class="text-center">
                    <label style="    font-size: 1.6rem;
    color: #3c8cbc;">{{$permission->category}}</label>
                    <hr style="    width: 149px;
    padding: 1px 2px;
    background-color: #3c8cbc;">
                </div>
            @endif
            @if($permission->category != $title)
                <div class="clearfix"></div>
                <br>
                <br>
                <div class="text-center">
                    <label style="    font-size: 1.6rem;
    color: #3c8cbc;">{{$permission->category}}</label>
                    <hr style="    width: 149px;
    padding: 1px 2px;
    background-color: #3c8cbc;">
                </div>
            @endif

            <div class="form-group col-lg-4" id="permissions_wrap">

                <div class="">
                    <input type="checkbox" name="permissions[]" value="{{$permission->id}}"
                            {{$model->hasPermissionTo($permission->name) ? 'checked' : ''}}>

                    <label for="permissions">{{$permission->display_name}}</label>
                </div>
            </div>

            @php
                $title = $permission->category;
            @endphp

        @endforeach
    </div>

    <div class="clearfix"></div>

</div>
<br>
<br>