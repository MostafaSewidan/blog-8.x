@extends('layouts.main',[
                                'page_header'       => 'الصلاحيات',
                                'page_description'  => '  صلاحية جديد ',
                                'link' => url('roles')
                                ])
@section('content')
        <!-- general form elements -->
<div class="box box-primary">
    <!-- form start -->
    {!! Form::model($model,[
                            'action'=>'Admin\RoleController@store',
                            'id'=>'myForm',
                            'role'=>'form',
                            'method'=>'POST',
                            'files' => true
                            ])!!}

    <div class="box-body">

        @include('roles.form')

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">حفظ</button>
        </div>

    </div>
    {!! Form::close()!!}

</div><!-- /.box -->

@endsection