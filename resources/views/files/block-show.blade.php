<div class="attachment">
    @php $count = 1; @endphp
    @foreach($records as $record)

        <div class="file-box" id="removable{{$record->id}}">
            <div class="file">
                <div class="ibox-tools">

                    @if($record->user_id == auth()->user()->id)
                        @can('edit_file')
                            <a onclick="openEditModel({{$record}})"><i
                                        class="fa fa-edit"></i></a>
                        @endcan

                        @can('delete_file')
                            <button
                                    id="{{$record->id}}"
                                    data-token="{{ csrf_token() }}"
                                    data-route="{{URL::route('folder.files.destroy',[$folder->id , $record->id])}}"
                                    type="button" class="destroy"
                                    style="color: #c7c7c7;background-color: #ffffff00;border: none;"
                            ><i class="fa fa-times"></i>
                            </button>
                        @endcan
                    @endif
                </div>
                <a href="{{url('file/'.$record->id.'/contents')}}">
                    <span class="corner"></span>

                    <div class="icon">
                        <i class="fa fa-file" style="color: #0f5e8d85;"></i>
                    </div>
                    <div class="file-name">
                        <span style="font-size: 1.6rem;">{{$record->name}}</span>
                        <span style="font-size: 1.6rem;float: left;">
                                            @if($record->status == 'private')
                                <i class="fa fa-lock"></i>
                            @else
                                <i class="fa fa-users"></i>
                            @endif
                                        </span>
                        <br>
                        <br>

                        <i class="fa fa-clock-o"></i>
                        <small>{{$record->created_at->locale('ar')->diffForHumans(Carbon\Carbon::now())}}</small>
                    </div>
                </a>
            </div>

        </div>

        @php $count ++; @endphp
    @endforeach
</div>