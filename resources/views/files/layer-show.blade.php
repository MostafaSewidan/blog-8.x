<div class="col-lg-12">
    @php $count = 1; @endphp
    @foreach($records as $record)
        <div class="vote-item" style="padding-top: 3px;padding-left: 2px;">
            <div class="ibox-tools">

                @if($record->user_id == auth()->user()->id)
                    @can('edit_file')
                        <a onclick="openEditModel({{$record}})"><i
                                    class="fa fa-edit"></i></a>
                    @endcan

                    @can('delete_file')
                        <button
                                id="{{$record->id}}"
                                data-token="{{ csrf_token() }}"
                                data-route="{{URL::route('folder.files.destroy',[$folder->id , $record->id])}}"
                                type="button" class="destroy"
                                style="color: #c7c7c7;background-color: #ffffff00;border: none;"
                        ><i class="fa fa-times"></i>
                        </button>
                    @endcan
                @endif
            </div>
            <div class="row">
                <div class="col-lg-10">
                    <a href="{{url('file/'.$record->id.'/contents')}}">
                        <div class="col-lg-2">
                            <div class="vote-icon">
                                <i class="fa fa-file" style="color: #0f5e8d85;"></i>
                            </div>
                        </div>
                        <p class="vote-title">
                            {{$record->name}}
                        </p>
                        <div class="vote-info">
                            @if($record->status == 'private')
                                <a href="#">خاص</a>
                                <i class="fa fa-lock"></i>
                            @else
                                <a href="#">عام</a>
                                <i class="fa fa-users"></i>
                            @endif
                            <a>
                                {{$record->created_at->locale('ar')->diffForHumans(Carbon\Carbon::now())}}
                            </a>
                            <i class="fa fa-clock-o"></i>

                        </div>
                    </a>
                </div>

            </div>
        </div>
        <br>
        @php $count ++; @endphp
    @endforeach
</div>