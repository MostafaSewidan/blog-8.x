@extends('layouts.main',[
								'page_header'		=> 'عرض المقالات',
								'page_description'	=> 'عرض ',
								'link' => url('file/'.$file->id.'/contents')
								])
@section('content')

    <div class="ibox box-primary">

        @can('add_content')
            <div class="ibox-title">
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{url('file/'.$file->id.'/contents/create')}}">
                        <i class="fa fa-plus"></i> اضافة جديد
                    </a>
                </div>
                <div class="clearfix"></div>
            </div>
        @endcan

        <div class="ibox-content">

            <div class="box-header">
                {!! Form::open([
                       'method' => 'get'
                       ]) !!}
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::text('id',request()->input('id'),[
                                'class' => 'form-control',
                                'placeholder' => 'رقم الملف'
                            ]) !!}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::text('name',request()->input('name'),[
                                'class' => 'form-control',
                                'placeholder' => 'إسم الملف'
                            ]) !!}
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::text('from',request()->input('from'),[
                                'class' => 'form-control datepicker',
                                'autocomplete' => 'off',
                                'placeholder' => 'من'
                            ]) !!}
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::text('to',\Request::input('to'),[
                                'class' => 'form-control datepicker',
                                'autocomplete' => 'off',
                                'placeholder' => 'إلى'
                            ]) !!}
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <button class="btn btn-flat btn-block btn-primary">بحث</button>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>

        </div>
        <br><br>
        @if(!empty($records) && count($records)>0)
            @php $count = 1; @endphp
            <div class="row">
                @foreach($records as $record)
                    <div class="col-lg-6 animated fadeInRight" id="removable{{$record->id}}">

                        <div class="mail-box-header" style="padding: 5px 0px 15px 4px;">
                            <label class="label label-lg label-inverse"
                                   style="float:right;font-size: 1.5rem;">#{{$record->id}}</label>
                            <div class="tooltip-demo" style="text-align: left;margin-bottom: -5px;">

                                @can('logs_index')

                                    <a href="{{url('logs?type=content&id='.$record->id)}}"
                                       class="btn btn-warning btn-sm"
                                       data-toggle="tooltip" data-placement="top" title=""
                                       data-original-title="Print email">
                                        <i class="fa fa-eye"></i> سجل العميلات
                                    </a>
                                @endcan

                                @can('edit_content')

                                    <a href="{{url('file/' . $file->id .'/contents/'.$record->id.'/edit')}}"
                                       class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top"
                                       title=""
                                       data-original-title="Print email">
                                        <i class="fa fa-pencil"></i> تعديل
                                    </a>
                                @endcan

                                @can('delete_content')

                                    <button
                                            id="{{$record->id}}"
                                            data-token="{{ csrf_token() }}"
                                            data-route="{{URL::route('file.contents.destroy',[$file->id , $record->id])}}"
                                            type="button"
                                            class="destroy btn btn-danger btn-sm">
                                        <i class="fa fa-trash-o"></i> حذف
                                    </button>
                                @endcan

                            </div>
                        </div>
                        <div class="mail-box-header">

                            <h2>
                                {{$record->title}}
                            </h2>
                            <div class="mail-tools tooltip-demo m-t-md">
                                <br>
                                <div class="user-friends">
                                    @if($record->creator)
                                        <span class="float-right font-normal">
                                            <strong>تمت الإضافه بواسطه :</strong>
                                            <a href="{{url('users?id='.$record->creator->id)}}">{{$record->creator->name}}</a>
                                        </span>
                                    @endif
                                    @if($record->users()->count())
                                        <br>
                                        <br>
                                        <p><strong>المرفقين :</strong></p>
                                        @foreach($record->users()->get() as $user)
                                            <a href="{{url('users?id='.$user->id)}}" title="test">
                                                <img alt="image" class="rounded-circle img-circle"
                                                     title="{{$user->name}}" src="{{$user->photo}}">
                                            </a>
                                        @endforeach
                                    @endif
                                </div>
                                <h5>
                                    <br>
                                    <span class="float-left font-normal" style="float: left">
                                    {{$record->created_at->locale('ar')->isoFormat('dddd  , MMMM  ,  Do / YYYY  ,  الساعة h:mm a')}}
                                </span>
                                </h5>
                            </div>
                        </div>
                        <div class="mail-box">


                            <div class="mail-body">
                                <p>
                                    <strong>التفاصيل</strong>
                                    <br>
                                    <br>
                                    {!! $record->description !!}
                                </p>
                            </div>

                            @if($record->attachmentRelation()->count())
                                <div class="mail-attachment">
                                    <p>

                        <span><i class="fa fa-paperclip"></i>
                            <span class="badge badge-primary"> {{$record->attachmentRelation()->count()}} </span>
                            المرفقات - </span>
                                        <a href="#">تحميل الكل</a>
                                    </p>

                                    <div class="attachment">

                                        @foreach($record->attachmentRelation()->get() as $attachment)
                                            <div class="file-box">
                                                <div class="file">
                                                    <a href="#">
                                                        <span class="corner"></span>

                                                        @if($attachment->type == 'file')
                                                            <div class="icon">
                                                                <i class="fa fa-file"></i>
                                                            </div>
                                                        @else
                                                            <div class="image">
                                                                <img alt="image" class="img-fluid img-responsive"
                                                                     src="{{url($attachment->path)}}">
                                                            </div>
                                                        @endif
                                                        <div class="file-name">
                                                            {{$attachment->name}}
                                                            <br>
                                                            <small>
                                                                أضيف :
                                                                {{$record->created_at->locale('ar')->isoFormat('dddd  , MMMM  ,  Do / YYYY  ,  الساعة h:mm a')}}
                                                            </small>
                                                        </div>
                                                    </a>
                                                </div>

                                            </div>
                                        @endforeach
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            @endif

                            @can('access_switch_content')
                                <div class="mail-body text-right tooltip-demo">
                                    <label class="label label-primary" style="font-size: 1.2rem;margin-left: 8px;">
                                        <i class="fa fa-newspaper-o"></i> حالة المقال
                                    </label>
                                    {!! \Helper\Helper::toggleBooleanView($record , url('file/'.$file->id.'/contents/'.$record->id.'/toggle-boolean/is_access'),'is_access') !!}

                                    <br><br>
                                    <p>(للقرأه فقط أو القرائه و التعديل)</p>
                                </div>
                            @endcan
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    @php $count ++; @endphp
                    @if($count % 2)
                        <div class="clearfix"></div>
                    @endif
                @endforeach
            </div>

            {!! $records->render() !!}
        @else
            <div class="alert alert-danger text-center">لا يوجد بيانات</div>
        @endif
    </div>
@stop

