@extends('layouts.main',[
                                'page_header'       => 'المقالات',
                                'page_description'  => 'تعديل ',
								'link' => url('file/'.$file->id.'/contents')
                                ])
@section('content')
    <!-- general form elements -->
    <div class="ibox ibox-primary">
        <!-- form start -->
        {!! Form::model($record,[
                                'url'=>url('file/'.$file->id.'/contents/'.$record->id),
                                'id'=>'ajaxForm',
                                'role'=>'form',
                                'method'=>'PUT',
                                'files' => true
                                ])!!}

        <div class="ibox-content">
            <div class="clearfix"></div>
            <br>
            @include('contents.form')

            <div class="ibox-footer">
                {!! \Helper\Field::ajaxBtn('حفظ') !!}
            </div>

        </div>
        {!! Form::close()!!}

    </div><!-- /.box -->

@endsection
