@extends('layouts.main',[
                                'page_header'       => 'المقالات',
                                'page_description'  => 'إضافة ',
								'link' => url('file/'.$file->id.'/contents')
                                ])

@section('content')
    <!-- general form elements -->
    <div class="ibox ibox-primary">
        <!-- form start -->
        {!! Form::model($record,[
                                'action'=>['Admin\FileContentController@store', $file->id],
                                'id'=>'ajaxForm',
                                'role'=>'form',
                                'method'=>'POST',
                                'files' => true
                                ])!!}

        <div class="ibox-content">

            @include('contents.form')

            <div class="ibox-footer">
                {!! \Helper\Field::ajaxBtn('حفظ') !!}
            </div>

        </div>
        {!! Form::close()!!}

    </div><!-- /.box -->

@endsection
