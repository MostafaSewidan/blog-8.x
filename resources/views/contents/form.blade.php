@inject('users','App\Models\User')

{!! \Helper\Field::checkBox('users','المستخدمين',$users->pluck('name','id')->toArray() ,
$record->users()->count() ? $record->users->pluck('id')->toArray():null) !!}

{!! \Helper\Field::text('title' , 'العنوان ')!!}
{!! \Helper\Field::editor('description' , 'المحتوي بالإنجليزية')!!}
{!! \Helper\Field::radio('is_access' , 'قابل للتعديل' , [1=>'نعم',0=>'لا'], $record->is_access)!!}


{!! \Helper\Field::multiFileUpload('attachments','المرفقات') !!}



@if($record->attachmentRelation()->count())
    <div class="mail-attachment">
        <p>
            <span><i class="fa fa-paperclip"></i>
                المرفقات  </span>
        </p>

        <div class="attachment">

            @foreach($record->attachmentRelation()->get() as $attachment)
                <div class="file-box" id="removable{{$attachment->id}}">
                    <div class="file">
                        <div class="ibox-tools">
                            <button
                                    id="{{$attachment->id}}"
                                    data-token="{{ csrf_token() }}"
                                    data-route="{{URL::route('attachment.destroy',$attachment->id)}}"
                                    type="button" class="destroy"
                                    style="color: #c7c7c7;background-color: #ffffff00;border: none;"
                            ><i class="fa fa-times"></i>
                            </button>

                        </div>
                        <a href="#">
                            <span class="corner"></span>

                            @if($attachment->type == 'file')
                                <div class="icon">
                                    <i class="fa fa-file"></i>
                                </div>
                            @else
                                <div class="image">
                                    <img alt="image" class="img-fluid img-responsive"
                                         src="{{url($attachment->path)}}">
                                </div>
                            @endif
                            <div class="file-name">
                                {{$attachment->name}}
                                <br>
                                <small>
                                    أضيف :
                                    {{$record->created_at->locale('ar')->isoFormat('dddd  , MMMM  ,  Do / YYYY  ,  الساعة h:mm a')}}
                                </small>
                            </div>
                        </a>
                    </div>

                </div>
            @endforeach
            <div class="clearfix"></div>
        </div>
    </div>
@endif
