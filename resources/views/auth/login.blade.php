<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name=“robots” content=“noindex”>
    <meta name=“googlebot” content=“noindex”/>

    <title> {{ config('app.name')}} | تسجيل الدخول </title>

    <link rel="icon" type="image/png" href="{{asset('photos/logo.svg')}}"/>
    <link href="{{asset('photos/logo.svg')}}" rel="apple-touch-icon">
    {{--favicons--}}
    <div class="text-center">
        <img src="{{asset('photos/logo.svg')}}" style="margin-top: 20px; margin-bottom:auto;" height="150"
             alt="logo">
    </div>
    <link href="{{asset('inspina/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('inspina/css/bootstrap-rtl.min.css')}}" rel="stylesheet">

    <link href="{{asset('inspina/font-awesome/css/font-awesome.css')}}" rel="stylesheet">

    <link href="{{asset('inspina/css/animate.css')}}" rel="stylesheet">

    <link href="{{asset('inspina/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('inspina/css/inspina-rtl.css')}}" rel="stylesheet">
    <style>
        body {
            font-family: "open sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
            background-color: #f3f3f4;
            font-size: 13px;
            overflow-x: hidden;
        }
    </style>
</head>

<body style="Xmargin-top: 4%;">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>

        {!! Form::open(['url'=>url('login'),'class'=>'m-t' , 'role'=>'form']) !!}

        <div class="form-group{{$errors->has('email') ? ' has-error' : '' }}">
            <input type="email" class="form-control" placeholder="البريد الالكتروني" required="" name="email" >
            @if (!empty($errors) && $errors->has('email'))
                <span class="help-block">
                    <i class="fa fa-info-circle" style="color: #ff3838;
    font-size: 1.5rem;"></i>
                                        <strong >{{ $errors->first('email') }}</strong>
                                    </span>
            @endif
        </div>
        <div class="form-group{{$errors->has('password') ? ' has-error' : '' }}">
            <input type="password" class="form-control" placeholder="كلمة المرور" required=""
                   name="password">
            @if ($errors->has('password'))
                <span class="help-block">
                    <i class="fa fa-info-circle" style="color: #ff3838;
    font-size: 1.5rem;"></i>
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
            @endif
        </div>

        <div class="form-group">
            <div class="checkbox">
                <label >
                    <input type="checkbox" name="remember" value="1"> تذكرني
                </label>
            </div>
        </div>

        <button type="submit" class="btn btn-primary block full-width m-b"
                style="">دخول
        </button>
        {!! Form::close() !!}
        <p class="m-t">
            <small> {{ config('app.name')}} &copy; {{date('Y')}}</small>
        </p>
    </div>
</div>

<!-- Mainly scripts -->
<script src="{{asset('public/inspina/js/jquery-2.1.1.js')}}"></script>
<script src="{{asset('public/inspina/js/bootstrap.min.js')}}"></script>

<!-- Custom and plugin javascript -->
<script src="{{asset('public/inspina/js/inspinia.js')}}"></script>
{{--<script src="{{asset('js/enjz.js')}}"></script>--}}
<script>

    @if( session()->get('success'))

    swal({
        title: "نجحت العملية!",
        text: '{{session('success')}}',
        type: "success",
        confirmButtonText: "حسناً"
    });


    @elseif(session()->get('fail'))

    swal({
        title: "فشلت العملية!",
        text: '{{session('fail')}}',
        type: "error",
        confirmButtonText: "حسناً"
    });

    @endif

</script>
</body>

</html>