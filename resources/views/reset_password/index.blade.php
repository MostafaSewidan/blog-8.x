@extends('layouts.main',[
                                    'page_header'       => 'تغيير كلمة المرور',
                                    'page_description'  => '  تعديل ',
                                    'link' => url('admin/reset-password')
                                ])
@section('content')
    <!-- general form elements -->
    <div class="box box-primary">
        <!-- form start -->
        {!! Form::open([
                                'url'=>url('reset-password/'),
                                'id'=>'ajaxForm',
                                'role'=>'form',
                                'method'=>'post',
                                'files' => true
                                ])!!}

        <div class="box-body">
            <div class="clearfix"></div>
            <br>

            {!! \Helper\Field::password('old_password' , 'كلمة المرور الحالية' )!!}
            {!! \Helper\Field::password('password' , 'كلمة المرور الجديدة' ) !!}
            {!! \Helper\Field::password('password_confirmation' , 'تاكيد كلمة المرور الجديدة' ) !!}


            <div class="box-footer">
                {!! \Helper\Field::ajaxBtn('حفظ') !!}
            </div>

        </div>
        {!! Form::close()!!}

    </div><!-- /.box -->

@endsection