<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserSharesTable extends Migration {

	public function up()
	{
		Schema::create('user_shares', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('shareable_type');
			$table->integer('shareable_id');
			$table->integer('user_id');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('user_shares');
	}
}