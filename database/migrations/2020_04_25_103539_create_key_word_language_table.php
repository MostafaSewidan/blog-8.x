<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateKeyWordLanguageTable extends Migration {

	public function up()
	{
		Schema::create('key_word_language', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('key_word_id');
			$table->integer('language_id');
			$table->string('word');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('key_word_language');
	}
}