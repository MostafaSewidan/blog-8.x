<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContentsTable extends Migration {

	public function up()
	{
		Schema::create('contents', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('file_id');
			$table->string('title');
			$table->text('description')->nullable();
			$table->tinyInteger('is_access')->default(1);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('contents');
	}
}