<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLanguagesTable extends Migration {

	public function up()
	{
		Schema::create('languages', function(Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->string('local_name');
            $table->tinyInteger('is_active')->default('1');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('languages');
	}
}