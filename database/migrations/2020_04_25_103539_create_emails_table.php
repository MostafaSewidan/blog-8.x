<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmailsTable extends Migration {

	public function up()
	{
		Schema::create('emails', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->string('title');
			$table->longText('content')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('emails');
	}
}