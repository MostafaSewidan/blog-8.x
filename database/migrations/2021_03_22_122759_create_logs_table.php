<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLogsTable extends Migration {

	public function up()
	{
		Schema::create('logs', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('user_id');
			$table->enum('action',['add','edit','remove'])->nullable();
			$table->string('title');
			$table->text('description')->nullable();
			$table->string('logable_type');
			$table->integer('logable_id');
		});
	}

	public function down()
	{
		Schema::drop('logs');
	}
}