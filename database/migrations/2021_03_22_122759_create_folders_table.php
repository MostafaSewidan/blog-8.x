<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Helper\Helper;
use App\Models\Folder;

class CreateFoldersTable extends Migration {

	public function up()
	{
		Schema::create('folders', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->string('name');
			$table->enum('status' , Helper::pluckArrayKey(Folder::$status))->default('public');
            $table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('folders');
	}
}