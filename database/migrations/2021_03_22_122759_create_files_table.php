<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Helper\Helper;
use App\Models\File;

class CreateFilesTable extends Migration {

	public function up()
	{
		Schema::create('files', function(Blueprint $table) {
			$table->increments('id');
            $table->integer('user_id');
			$table->integer('folder_id');
			$table->string('name');
            $table->enum('status' , Helper::pluckArrayKey(File::$status))->default('public');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('files');
	}
}