<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContentUserTable extends Migration {

	public function up()
	{
		Schema::create('content_user', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('content_id');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('content_user');
	}
}