<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class permission extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert("
            INSERT INTO `permissions` (`id`, `category`, `name`, `guard_name`, `display_name`, `routes`, `order`, `created_at`, `updated_at`) VALUES
(9, 'المرفقات', 'delete_attachment', 'admin', 'حذف', 'attachment.destroy', 7, '2021-03-24 15:24:40', '2021-03-24 17:04:55'),
(10, 'الصلاحيات', 'test28', 'admin', 'مشاهدة البيانات', 'roles.index,roles.show', 1, '2021-03-24 15:24:40', '2021-03-24 17:04:55'),
(11, 'الصلاحيات', 'test29', 'admin', 'إضافة', 'roles.create,roles.store', 1, '2021-03-24 15:24:40', '2021-03-24 17:04:55'),
(14, 'الصلاحيات', 'test32', 'admin', 'تعديل', 'roles.edit,roles.update', 1, '2021-03-24 15:24:40', '2021-03-24 17:04:55'),
(16, 'الصلاحيات', 'test34', 'admin', 'حذف', 'roles.destroy', 1, '2021-03-24 15:24:40', '2021-03-24 17:04:55'),
(17, 'المستخدمين', 'test35', 'admin', 'تفعيل وإلفاء تفعيل', 'users.toggleBoolean', 2, '2021-03-24 15:24:40', '2021-03-24 17:04:55'),
(18, 'المستخدمين', 'test36', 'admin', 'مشاهدة البيانات', 'users.index,users.show', 2, '2021-03-24 15:24:40', '2021-03-24 17:04:55'),
(19, 'المستخدمين', 'test37', 'admin', 'إضافة', 'users.create,users.store', 2, '2021-03-24 15:24:40', '2021-03-24 17:04:55'),
(22, 'المستخدمين', 'test40', 'admin', 'تعديل', 'users.edit,users.update', 2, '2021-03-24 15:24:40', '2021-03-24 17:04:55'),
(24, 'المستخدمين', 'test42', 'admin', 'حذف', 'users.destroy', 2, '2021-03-24 15:24:40', '2021-03-24 17:04:55'),
(25, 'الإعدادات', 'test43', 'admin', 'مشاهده', 'settings-view', 8, '2021-03-24 15:24:40', '2021-03-24 15:38:54'),
(26, 'الإعدادات', 'test44', 'admin', 'تعديل', 'settings-save', 8, '2021-03-24 15:24:40', '2021-03-24 15:38:54'),
(41, 'سجلات العمليات', 'logs_index', 'admin', 'مشاهدة البيانات', 'logs.index', 3, '2021-03-24 15:24:41', '2021-03-24 17:04:55'),
(48, 'المجلدات', 'show_folders', 'admin', 'مشاهدة البيانات', 'folders.index,folders.show', 4, '2021-03-24 15:24:41', '2021-03-24 17:04:55'),
(49, 'المجلدات', 'add_folder', 'admin', 'إضافة', 'folders.create,folders.store', 4, '2021-03-24 15:24:41', '2021-03-24 17:04:55'),
(52, 'المجلدات', 'edit_folder', 'admin', 'تعديل', 'folders.edit,folders.update', 4, '2021-03-24 15:24:41', '2021-03-24 17:04:55'),
(54, 'المجلدات', 'delete_folder', 'admin', 'حذف', 'folders.destroy', 4, '2021-03-24 15:24:41', '2021-03-24 17:04:55'),
(55, 'الملفات', 'show_files', 'admin', 'مشاهدة البيانات', 'folder.files.index,folder.files.show', 5, '2021-03-24 15:24:41', '2021-03-24 17:04:55'),
(56, 'الملفات', 'add_file', 'admin', 'إضافة', 'folder.files.create,folder.files.store', 5, '2021-03-24 15:24:41', '2021-03-24 17:04:55'),
(59, 'الملفات', 'edit_file', 'admin', 'تعديل', 'folder.files.edit,folder.files.update', 5, '2021-03-24 15:24:41', '2021-03-24 17:04:55'),
(61, 'الملفات', 'delete_file', 'admin', 'حذف', 'folder.files.destroy', 5, '2021-03-24 15:24:41', '2021-03-24 17:04:55'),
(62, 'المقالات', 'access_switch_content', 'admin', 'للقرأه فقط أو القرائه و التعديل', 'contents.toggleBoolean', 6, '2021-03-24 15:24:41', '2021-03-24 17:04:55'),
(63, 'المقالات', 'show_all_contents', 'admin', 'مشاهدة البيانات', 'asd', 6, '2021-03-24 15:24:41', '2021-03-24 17:04:55'),
(64, 'المقالات', 'add_content', 'admin', 'إضافة', 'file.contents.create,file.contents.store', 6, '2021-03-24 15:24:41', '2021-03-24 17:04:55'),
(67, 'المقالات', 'edit_content', 'admin', 'تعديل', 'file.contents.edit,file.contents.update', 6, '2021-03-24 15:24:41', '2021-03-24 17:04:55'),
(69, 'المقالات', 'delete_content', 'admin', 'حذف', 'file.contents.destroy', 6, '2021-03-24 15:24:41', '2021-03-24 17:04:55'),
(70, 'المقالات', 'show_his_contents', 'admin', 'مشاهدة المقالات الخاصه به فقط', 'asd', 6, '2021-03-24 15:24:41', '2021-03-24 17:04:55');
        ");

        DB::insert("
        
INSERT INTO `roles` (`id`, `name`, `guard_name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', 'المدير', '2021-03-24 17:11:39', '2021-03-24 17:11:39'),
(3, 'user', 'admin', 'مستخدم', '2021-03-24 17:54:14', '2021-03-25 05:34:00');
        ");


        Role::findOrFail(1)->permissions()->attach(\Spatie\Permission\Models\Permission::pluck('id')->toArray());
        User::find(1)->assignRole('admin');
    }
}
