<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//Auth::routes();

Route::group(['middleware' => ['guest:admin']], function () {
    Route::get('/login', 'AuthController@viewLogin')->name('login');
    Route::post('/login', 'AuthController@login');
});

Route::group(['middleware' => ['auth:admin', 'admin-active', 'auto-check-permission']], function () {

    Route::post('/logout', 'AuthController@logout')->name('logout');
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('attachment', 'AttachmentController');

    Route::get('reset-password', 'AuthController@ResetPasswordView');
    Route::post('reset-password', 'AuthController@ResetPassword');

    Route::resource('roles', 'RoleController');

    Route::get('users/toggle-boolean/{id}/{action}', 'UserController@toggleBoolean')->name('users.toggleBoolean');
    Route::resource('users', 'UserController');

    Route::get('settings', 'SettingsController@view')->name('settings-view');
    Route::post('settings', 'SettingsController@update')->name('settings-save');

    Route::resource('developer/setting', 'DeveloperSetting');
    Route::resource('developer/settings/categories', 'SettingCategoryController');

    //notifications
    Route::get('notifications/read/{id}', 'NotificationController@read');
    Route::resource('notifications', 'NotificationController');

    //logs
    Route::resource('logs', 'LogController');

    //emails
    Route::resource('emails', 'EmailController');

    //folders
    Route::resource('folders', 'FolderController');

    //files
    Route::resource('folder.files', 'FolderFileController');

    //files
    Route::get('file/{file}/contents/{content}/toggle-boolean/{action}', 'FileContentController@toggleBoolean')->name('contents.toggleBoolean');
    Route::resource('file.contents', 'FileContentController');
});