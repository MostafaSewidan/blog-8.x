

$(document).on('click','.destroy',function(){
    var route   = $(this).data('route');
    var token   = $(this).data('token');
    $.confirm({
        icon                : 'glyphicon glyphicon-floppy-remove',
        animation           : 'rotateX',
        closeAnimation      : 'rotateXR',
        title               : 'تأكد عملية الحذف',
        autoClose           : 'cancel|6000',
        text             : 'هل أنت متأكد من الحذف ؟',
        confirmButtonClass  : 'btn-outline',
        cancelButtonClass   : 'btn-outline',
        confirmButton       : 'نعم',
        cancelButton        : 'لا',
        dialogClass			: "modal-danger modal-dialog",
        confirm: function () {
            $.ajax({
                url     : route,
                type    : 'post',
                data    : {_method: 'delete', _token :token},
                dataType:'json',
                success : function(data){
                    if(data.status === 0)
                    {
                        swal({
                            title: "فشلت العملية!",
                            text: data.message,
                            type: "error",
                            confirmButtonText: "حسناً"
                        });

                    }else{
                        $("#removable"+data.id).remove();
                        swal({
                            title: "نجحت العملية!",
                            text: data.message,
                            type: "success",
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                }
            });
        },
    });
});

$('.select2').select2({
    dir: "rtl"
});

$('.datepicker').datepicker({
    dateFormat: 'yy-mm-dd',
    changeMonth: true,
    // yearRange: “c-70:c+10”,
    changeYear: true
});
// $('.datetimepicker').datetimepicker();
// initialize with defaults
$(".file_upload_preview").fileinput({
    showUpload: false,
    showRemove: false,
    showCaption: false
});


// /***
//  * ajax request
//  * ****/
// $("#governorate").change(function () {
//     let governorate   = $("#governorate").val();
//     let url   = window.laravelUrl+"/api/cities?governorate_id="+governorate;
//     $.ajax({
//         url     : url,
//         type    : 'get',
//         dataType:'json',
//         success : function(data){
//             $('#city').empty();
//             let option = '<option value="">اختر المدينة</option>';
//             $("#city").append(option);
//             $.each(data.data, function( index, city ) {
//                 let option = '<option value="'+city.id+'">'+city.name+'</option>';
//                 $("#city").append(option);
//             });
//         }
//     });
// });
//
// $("#city").change(function () {
//     let city   = $("#city").val();
//     let url   = window.laravelUrl+"/api/regions?city_id="+city;
//     $.ajax({
//         url     : url,
//         type    : 'get',
//         dataType:'json',
//         success : function(data){
//             $('#region').empty();
//             let option = '<option value="">اختر المنطقة</option>';
//             $("#region").append(option);
//             $.each(data.data, function( index, region ) {
//                 let option = '<option value="'+region.id+'">'+region.name+'</option>';
//                 $("#region").append(option);
//             });
//         }
//     });
// });


function printData()
{
    var divToPrint=document.getElementById("printArea");
    var newWin= window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
}


$("#governorate").change(function () {
    var governorate   = $("#governorate").val();
    var route = $("#url").val();
    var url = route+"/api/v1/client/cities?governorate_id="+governorate;
    $.ajax({
        url     : url,
        type    : 'get',
        dataType:'json',
        success : function(data){
            $('#city').empty();
            var option = '<option value="">اختر المدينة</option>';
            $("#city").append(option);
            $.each(data.data, function( index, city ) {
                var option = '<option value="'+city.id+'">'+city.name+'</option>';
                $("#city").append(option);
            });
        }
    });
});

// $("#city").change(function () {
//     var city   = $("#city").val();
//     var url   = window.laravelUrl+"/api/v1/regions?city_id="+city;
//     $.ajax({
//         url     : url,
//         type    : 'get',
//         dataType:'json',
//         success : function(data){
//             $('#region').empty();
//             var option = '<option value="">اختر المنطقة</option>';
//             $("#region").append(option);
//             $.each(data.data, function( index, region ) {
//                 var option = '<option value="'+region.id+'">'+region.name+'</option>';
//                 $("#region").append(option);
//             });
//         }
//     });
// });