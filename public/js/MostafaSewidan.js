// function push_element(name) {
//
//
//     console.log(name);
//     var input_value = $("#" + name + "").val();
//     var input_type = $("#" + name + "").attr('type');
//     console.log(input_value);
//     if (input_value === '' || input_value == null) {
//         $("#" + name + "").focus();
//
//     } else {
//         var uuid = Math.floor((Math.random() * 100) + 1);
//         var row_built = '<tr id="removable' + uuid + '">';
//
//         row_built += '<td><div class="form-group"><input class="form-control" name="' + name + '[]" value="' + input_value + '" type="' + input_type + '"></div></td>';
//         row_built += '<td class="text-center"><a href="javascript:void(0)" id="' + uuid + '" class="delete-row btn btn-danger"><i class="fa fa-trash"></i></a></td>';
//         row_built += '</tr>';
//         $("#" + name + "_table_body").append(row_built);
//         $("#" + name + "").val("");
//     }
// }

function printDiv(id)
{
    var divToPrint=document.getElementById(id);
    var newWin=window.open('','Print-Window');

    newWin.document.open();

    newWin.document.write('<html><body onload="window.print()"><center>'+divToPrint.innerHTML+'</center></body></html>');

    newWin.document.close();

    setTimeout(function(){newWin.close();},10);

}


$(".file_upload_preview").fileinput({
    showUpload: false,
    showRemove: false,
    showCaption: false
});

function toggle(source, name) {
    checkboxes = document.getElementsByName(name + '[]');
    for (var i = 0, n = checkboxes.length; i < n; i++) {
        checkboxes[i].checked = source.checked;
    }
}

function initSingleSwitchery(elem) {
    var init = new Switchery(elem, {size: 'small'});
}


// js switchery multiple
function initSwichery() {
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

    elems.forEach(function (html) {
        var switchery = new Switchery(html, {size: 'small'});
    });
}

initSwichery();

function toggleBoolean(el, url) {
    var checked = $(el).is(':checked');
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        success: function (data) {
            if (data.status === 0) {
                $(el).prop('checked', !checked);
                $(el).next().remove();
                initSingleSwitchery(el);
                $("#removable" + data.id).remove();
                swal({
                    title: "فشلت العملية!",
                    text: data.massage,
                    type: "error",
                    confirmButtonText: "حسناً"
                });
            }

        }, error: function () {
            $(el).prop('checked', !checked);
            $(el).next().remove();
            initSingleSwitchery(el);
            Swal.fire("خطأ!", "حدث خطأ", "error");
        }
    });
}

function getCode(url) {
    $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        success: function (data) {
            if (data.status === 1) {
                $('#code').val(data.code);
            }

        }, error: function () {
        }
    });
}

$('.table_body').on('click', 'a.delete-row', function (e) {

    console.log(10000);
    e.preventDefault();
    console.log(this.id);
    var elToDel = $("#removable" + this.id);
    console.log(elToDel);
    elToDel.remove();
});

$(document).ready(function () {

    //console.log(1);

});


function contact_read(id) {

    $.ajax({
        url: 'contacts/is-read/' + id,
        type: 'get',
        success: function (data) {

            $('#removable' + id).css('background-color', 'white');
        },
        error: function (data) {

        }
    });
}

function readNotification(url) {

    $.ajax({
        url: url,
        type: 'get',
        success:function(data){

            // console.log(data);

        },
        error: function (data) {

            // console.log(data);

        }
    });

}
function toggleHomeSpinners() {

    $('#sedar-div').toggle();
}


$('#ajaxForm').submit(function (e) {

    e.preventDefault();
    toggleHomeSpinners();
    var form = $('#ajaxForm');
    var url = form.attr('action');
    var form_data = new FormData(this);
    var form_type = form.attr('method');
    var btn = $('#ajax-button');
    var btnSpinner = $('#btn-spinner');
    var btnName = $('#btn-name');

    $(".help-block").hide();
    btnName.toggle();
    $(".has-error").removeClass('has-error');

    btn.attr('disabled', 'disabled');
    btnSpinner.show();
    $.ajax({
        url: url,
        type: form_type,
        data: form_data,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {

            if(data.status === 0)
            {
                var error = data.errors;
                btnName.toggle();
                btn.removeAttr('disabled');
                btnSpinner.hide();
                toggleHomeSpinners();

                $.each(error, function (key, value) {
                    var targetHelpBlock = $('#' + key + '_error');
                    targetHelpBlock.text('').append(value);
                    targetHelpBlock.parent().show();
                    targetHelpBlock.parent().parent().addClass('has-error');

                });

            }else{
                var url = data.url;
                if (url) {
                    window.location = url;
                }
            }
        },
        error: function (data) {
            var error = data.responseJSON.errors;

            btn.removeAttr('disabled');
            btnSpinner.hide();
            toggleHomeSpinners();

            $.each(error, function (key, value) {
                var targetHelpBlock = $('#' + key + '_error');
                targetHelpBlock.text('').append(value);
                targetHelpBlock.parent().show();
                targetHelpBlock.parent().parent().addClass('has-error');

            });

        }
    });
});



/**
 * Show a confirmation dialog
 * @param [options] {{title, text, confirm, cancel, confirmButton, cancelButton, post, submitForm, confirmButtonClass}}
 * @param [e] {Event}
 */
$.confirm = function (options, e) {
    // Do nothing when active confirm modal.
    if ($('.confirmation-modal').length > 0)
        return;

    // Parse options defined with "data-" attributes
    var dataOptions = {};
    if (options.button) {
        var dataOptionsMapping = {
            'title': 'title',
            'text': 'text',
            'confirm-button': 'confirmButton',
            'submit-form': 'submitForm',
            'cancel-button': 'cancelButton',
            'confirm-button-class': 'confirmButtonClass',
            'cancel-button-class': 'cancelButtonClass',
            'dialog-class': 'dialogClass'
        };
        $.each(dataOptionsMapping, function(attributeName, optionName) {
            var value = options.button.data(attributeName);
            if (value) {
                dataOptions[optionName] = value;
            }
        });
    }

    // Default options
    var settings = $.extend({}, $.confirm.options, {
        confirm: function () {
            if (dataOptions.submitForm){
                e.target.closest("form").submit();
            } else {
                var url = e && (('string' === typeof e && e) || (e.currentTarget && e.currentTarget.attributes['href'].value));
                if (url) {
                    if (options.post) {
                        var form = $('<form method="post" class="hide" action="' + url + '"></form>');
                        $("body").append(form);
                        form.submit();
                    } else {
                        window.location = url;
                    }
                }
            }
        },
        cancel: function (o) {
        },
        button: null
    }, dataOptions, options);

    // Modal
    var modalHeader = '';
    if (settings.title !== '') {
        modalHeader =
            '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
            '<h4 class="modal-title">' + settings.title+'</h4>' +
            '</div>';
    }
    var modalHTML =
        '<div class="confirmation-modal modal fade" tabindex="-1" role="dialog">' +
        '<div class="'+ settings.dialogClass +'">' +
        '<div class="modal-content">' +
        modalHeader +
        '<div class="modal-body">' + settings.text + '</div>' +
        '<div class="modal-footer">' +
        '<button class="confirm btn ' + settings.confirmButtonClass + '" type="button" data-dismiss="modal">' +
        settings.confirmButton +
        '</button>' +
        '<button class="cancel btn ' + settings.cancelButtonClass + '" type="button" data-dismiss="modal">' +
        settings.cancelButton +
        '</button>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';

    var modal = $(modalHTML);

    modal.on('shown.bs.modal', function () {
        modal.find(".btn-primary:first").focus();
    });
    modal.on('hidden.bs.modal', function () {
        modal.remove();
    });
    modal.find(".confirm").click(function () {
        settings.confirm(settings.button);
    });
    modal.find(".cancel").click(function () {
        settings.cancel(settings.button);
    });

    // Show the modal
    $("body").append(modal);
    modal.modal('show');
};

$(document).on('click','.destroy',function(){
    var route   = $(this).data('route');
    var token   = $(this).data('token');
    $.confirm({
        icon                : 'glyphicon glyphicon-floppy-remove',
        animation           : 'rotateX',
        closeAnimation      : 'rotateXR',
        title               : 'تأكد عملية الحذف',
        autoClose           : 'cancel|6000',
        text             : 'هل أنت متأكد من الحذف ؟',
        confirmButtonClass  : 'btn-outline',
        cancelButtonClass   : 'btn-outline',
        confirmButton       : 'نعم',
        cancelButton        : 'لا',
        dialogClass			: "modal-danger modal-dialog",
        confirm: function () {
            $.ajax({
                url     : route,
                type    : 'post',
                data    : {_method: 'delete', _token :token},
                dataType:'json',
                success : function(data){
                    if(data.status === 0)
                    {
                        swal({
                            title: "فشلت العملية!",
                            text: data.message,
                            type: "error",
                            confirmButtonText: "حسناً"
                        });
                    }else{
                        $("#removable"+data.id).remove();
                        swal({
                            title: "نجحت العملية!",
                            text: data.message,
                            type: "success",
                            showConfirmButton: false,
                            timer: 1500
                        });
                    }
                }
            });
        },
    });
});