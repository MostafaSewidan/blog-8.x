<?php

namespace App\Traits;

use App\Models\Log;

trait LogTrait
{
    public function logs()
    {
        return $this->morphMany(Log::class, 'logable');
    }
}