<?php

namespace App\Traits;

use App\Models\File;
use App\Models\Folder;

trait Query
{
    public $request;

    public function folderPermission($request = null)
    {
        $this->request = $request ? $request : $this->request;
        $user = auth()->user();
        $query = Folder::where(function ($q) use ($user) {
            $q->where(function ($q) use ($user) {

                if ($user->can('show_his_contents') && !$user->can('show_all_contents')) {
                    $q->whereHas('files', function ($q) use ($user) {
                        $q->whereHas('contents', function ($q) use ($user) {
                            $q->whereHas('users', function ($q) use ($user) {
                                $q->where('user_id', $user->id);
                            });
                        });
                    });
                }

            })->orWhere(function ($q) use ($user) {

                if ($this->request->folder_search_status == 'private') {
                    $q->where(['status' => 'private', 'user_id' => $user->id]);

                } elseif ($this->request->folder_search_status == 'public') {

                    $q->where(['status' => 'public']);

                } else {

                    $q->where(['status' => 'private', 'user_id' => $user->id])->orWhere('status', 'public');
                }
            });
        });

        return $query;
    }

    public function filePermission($folder = null , $request = null)
    {
        $this->request = $request ? $request : $this->request;
        $user = auth()->user();
        if ($folder)
        {
            $query = $folder->files();
        }else{
            $query = File::where(function ($q) use ($user) {
                $q->whereHas('folder' , function ($q) use ($user) {
                    $q->where(['status' => 'public'])->orWhere(['status' => 'private', 'user_id' => $user->id]);
                });
            });
        }

        $query = $query->where(function ($q) use ($user) {

            $q->where(function ($q) use ($user) {

                if (auth()->user()->can('show_his_contents') && !auth()->user()->can('show_all_contents')) {

                    $q->whereHas('contents', function ($q) {
                        $q->whereHas('users', function ($q) {
                            $q->where('user_id' , auth()->user()->id);
                        });
                    });
                }

            })->orWhere(function ($q) use ($user) {

                if ($this->request->file_search_status == 'private') {
                    $q->where(['status' => 'private', 'user_id' => $user->id]);

                } elseif ($this->request->file_search_status == 'public') {

                    $q->where(['status' => 'public']);

                } else {

                    $q->where(['status' => 'private', 'user_id' => $user->id])->orWhere('status', 'public');
                }
            });
        });

        return $query;
    }
}