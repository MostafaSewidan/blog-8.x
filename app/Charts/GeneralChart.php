<?php

declare(strict_types = 1);

namespace App\Charts;

use App\Models\File;
use App\Models\Folder;
use Chartisan\PHP\Chartisan;
use ConsoleTVs\Charts\BaseChart;
use Illuminate\Http\Request;

class GeneralChart extends BaseChart
{
    /**
     * @param Request $request
     * @return Chartisan
     */
    public function handler(Request $request): Chartisan
    {
        $private_folders = Folder::where('status' , 'private')->count();

        $public_folders = Folder::where('status' , 'public')->count();

        $private_files = File::where('status' , 'private')->count();

        $public_files = File::where('status' , 'public')->count();

        $folderPieChart = Chartisan::build()
            ->labels([ 'المجلدات الخاصة', 'المجلدات العامة'])
            ->dataset("المجلدات",'pie',[$private_folders,$public_folders])
            ->backgroundcolor(["rgb(76, 175, 80)","rgb(175, 20, 20)"]);

        $filePieChart = Chartisan::build()
            ->labels([ 'الملفات الخاصة', 'الملفات العامة'])
            ->dataset('pie',[$private_files,$public_files])
            ->backgroundcolor(["rgb(76, 175, 80)","rgb(175, 20, 20)"]);


        return Chartisan::build()
            ->labels(['First', 'Second', 'Third'])
            ->dataset('Sample', [1, 2, 3])
            ->dataset('Sample 2', [3, 2, 1]);

        return [
            'folder_chart' => $folderPieChart,
            'file_chart' => $filePieChart
        ];
    }
}