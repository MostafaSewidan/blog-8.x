<?php

namespace App\Models;

use App\Traits\GetAttribute;
use App\Traits\LogTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles, GetAttribute,LogTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guard_name = 'admin';
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function notifications()
    {
        return $this->morphToMany(Notification::class,'notifiable')->withPivot('is_read');
    }

    public function folders(){
        return $this->hasMany(Folder::class);
    }

    public function files(){
        return $this->hasMany(File::class);
    }

    public function emails()
    {
        return $this->hasMany(Email::class);
    }

    public function sharedFolders(){
        return $this->morphedByMany(Folder::class,'sharable','user_shares');
    }

    public function sharedFiles(){
        return $this->morphedByMany(File::class,'sharable','user_shares');
    }

    public function getNewNotifiCountAttribute()
    {
        return $this->notifications()->wherePivot('is_read', 0)->count();
    }

    public function getRolesListAttribute()
    {
        return $this->roles()->pluck('guard_name', 'id')->toArray();
    }

    public function getPhotoAttribute()
    {
        return $this->attachmentRelation()->count() ? asset($this->attachmentRelation()->first()->path) : asset('inspina/img/default.png');
    }
}
