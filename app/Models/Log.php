<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{

    protected $table      = 'logs';
    public    $timestamps = true;
    protected $fillable   = array('title', 'description', 'user_id', 'action');

    public function logable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getModelUrlAttribute()
    {
        return $this->logSwitch('model_url');
    }

    public function getUserUrlAttribute()
    {
        return $this->logSwitch('user_url');
    }

    public function getTypeTextAttribute()
    {
        return $this->logSwitch('type_text');
    }

    public function getUserAttribute()
    {
        $relation = $this->type;
        return  $this->$relation;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function logSwitch($key)
    {
        $type_array  = explode('\\' , $this->logable_type);
        $type = lcfirst($type_array[count($type_array) - 1]);

        switch ($type) {
            case 'content':
                $array = [
                    'model_url' => url('file/'. $this->logable->file_id.'/contents?id=' . $this->logable_id),
                    'type_text' => 'مستخدم لوحة التحكم',
                    'user_url' => url('user?id='. $this->user_id),
                ];
                break;
            default:
                $array = [
                    'model_url' => url('not-found/'),
                    'type_text' => 'غير معروف',
                    'user_url' => url('home'),
                ];
                break;

        }

        return $array[$key];
    }

    public static function createLog($log_model , $user , $title , $description = null , $action = 'edit')
    {
        $log_model->logs()->create([
            'user_id' => $user->id,
            'title' => $title,
            'description' => $description,
            'action' => $action,
        ]);
    }

}
