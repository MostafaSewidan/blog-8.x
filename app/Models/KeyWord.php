<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KeyWord extends Model
{

    protected $table = 'key_words';
    public $timestamps = true;
    protected $fillable = array('key');

    public function languages()
    {
        return $this->belongsToMany(Language::class)->withPivot('word');
    }
}