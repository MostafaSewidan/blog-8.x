<?php

namespace App\Models;

use App\Traits\LogTrait;
use Illuminate\Database\Eloquent\Model;

class Folder extends Model 
{
    use LogTrait;

    static $status = [
        'private' => 'خاص',
        'public' => 'عام'
    ];
    protected $table = 'folders';
    public $timestamps = true;
    protected $fillable = array('name','status','user_id');

    public function files()
    {
        return $this->hasMany('App\Models\File');
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    public function users()
    {
        return $this->morphToMany(User::class , 'sharable','user_shares');
    }

}