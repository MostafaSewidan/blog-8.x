<?php

namespace App\Models;

use App\Traits\LogTrait;
use Illuminate\Database\Eloquent\Model;

class File extends Model 
{
    use LogTrait;

    static $status = [
        'private' => 'خاص',
        'public' => 'عام'
    ];

    protected $table = 'files';
    public $timestamps = true;
    protected $fillable = array('folder_id', 'name','status','user_id');

    public function folder()
    {
        return $this->belongsTo('App\Models\Folder');
    }

    public function contents()
    {
        return $this->hasMany('App\Models\Content');
    }

    public function creator()
    {
        return $this->belongsTo(User::class);
    }

    public function users()
    {
        return $this->morphToMany(User::class , 'sharable','user_shares');
    }

}