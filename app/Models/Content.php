<?php

namespace App\Models;

use App\Traits\GetAttribute;
use App\Traits\LogTrait;
use Illuminate\Database\Eloquent\Model;

class Content extends Model 
{
    use LogTrait;
    use GetAttribute;

    protected $table = 'contents';
    public $timestamps = true;
    protected $fillable = array('file_id', 'title','description');


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->multiple_attachment = true;
    }

    public function notifications()
    {
        return $this->morphMany(Notification::class , 'notifiable');
    }

    public function file()
    {
        return $this->belongsTo('App\Models\File');
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function getCreatorAttribute()
    {
        return $this->logs()->where('action' , 'add')->first() ? User::find($this->logs()->where('action' , 'add')->first()->user_id) : null;
    }

}