<?php

namespace App\Models;

use App\Traits\LogTrait;
use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    use LogTrait;

    protected $table = 'emails';
    public $timestamps = true;
    protected $fillable = array('title', 'content','user_id');

    public function sender()
    {
        return $this->belongsTo(User::class,'user_id');
    }

}