<?php

namespace Admin;

use App\Models\Folder;
use App\Traits\Master;
use App\Traits\Query;
use Illuminate\Support\Facades\Artisan;
use Spatie\Permission\Models\Permission;
use \Spatie\Permission\Models\Role;
use Helper\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;

class FolderController extends Controller
{
    use Master,Query;

    public function __construct()
    {
        $this->model = new Folder();
        $this->viewsDomain = 'folders.';
        $this->viewsUrl = 'folders';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->request = $request;

        $records = $this->folderPermission()->where(function ($q) use ($request) {

            if ($request->name) {
                $q->where(function ($q) use ($request) {

                    $q->where('name', 'LIKE', '%' . $request->name . '%');
                });
            }

            if ($request->from) {
                $q->whereDate('created_at', '>=', Helper::convertDateTime($request->from));
            }

            if ($request->to) {
                $q->whereDate('created_at', '<=', Helper::convertDateTime($request->to));
            }


        })->latest()->paginate(30)->appends(\Request::except('page'));

        return $this->view('index', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();
        $rules =
            [
                'name' => 'required|unique:folders,name',
                'status' => 'required|in:public,private',
            ];

        $message =
            [
                'name.required' => 'الرجاء ادخال الاسم',
                'status.required' => 'الرجاء إختيار حالة المجلد',
                'name.unique' => 'يوجد المجلد بهذا الإسم بالفعل',

            ];

        $data = validator()->make($request->all(), $rules, $message);

        if ($data->fails())
            return $this->returnError($data);


        $record = $user->folders()->create($request->all());

        session()->flash('success', 'تمت الاضافة بنجاح');
        return $this->returnSuccess();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = auth()->user();
        $record = $user->folders()->findOrFail($id);


        $rules =
            [
                'edit_name' => 'required|unique:folders,name,'.$id,
                'edit_status' => 'required|in:public,private',
            ];

        $message =
            [
                'edit_name.required' => 'الرجاء ادخال الاسم',
                'edit_status.required' => 'الرجاء إختيار حالة المجلد',
                'edit_name.unique' => 'يوجد المجلد بهذا الإسم بالفعل',

            ];
        $data = validator()->make($request->all(), $rules, $message);

        if ($data->fails()) {
            session()->flash('fail', 'الرجاء ادخال الاسم');
            return back();
        }

        $request->merge([
            'name' => $request->edit_name,
            'status' => $request->edit_status,
        ]);
        $record->update($request->all());

        session()->flash('success', 'تمت التعديل بنجاح');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = auth()->user();
        $record = $user->folders()->findOrFail($id);

        if ($record->files()->count()) {
            $data = [
                'status' => 0,
                'msg' => ' فشل الحذف المجلد يحتوي علي بعض الملفات',
                'id' => $id
            ];
            return Response::json($data, 200);
        }

        $record->delete();

        $data = [
            'status' => 1,
            'msg' => 'تم الحذف بنجاح',
            'id' => $id
        ];
        return Response::json($data, 200);
    }
}