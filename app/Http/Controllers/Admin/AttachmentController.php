<?php

namespace Admin;

use App\Http\Controllers\Controller;
use App\Models\Attachment;
use Illuminate\Support\Facades\File;
use Response;

class AttachmentController extends Controller
{

    public function destroy($id)
    {
        $attachment = Attachment::findOrFail($id);
        File::delete(public_path() . '/' . $attachment->path);
        $attachment->delete();
        $data = ['status' => 1, 'msg' => 'تم الحذف بنجاح', 'id' => $attachment->id];
        return response()->json($data, 200);
    }


}
