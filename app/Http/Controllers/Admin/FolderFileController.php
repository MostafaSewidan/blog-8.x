<?php

namespace Admin;

use App\Models\File;
use App\Models\Folder;
use App\Traits\Master;
use App\Traits\Query;
use Illuminate\Support\Facades\Artisan;
use Spatie\Permission\Models\Permission;
use \Spatie\Permission\Models\Role;
use Helper\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;

class FolderFileController extends Controller
{
    use Master,Query;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->model = new File();
        $this->viewsDomain = 'files.';
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($id , Request $request)
    {
        $folders = $this->folderPermission()->take(5)->get();
        $folder = $this->folderPermission()->findOrFail($id);

        $records = $this->filePermission($folder)->where(function($q) use($request){

            if($request->name)
            {
                $q->where(function ($q) use($request){

                    $q->where('name','LIKE','%'.$request->name.'%');
                });
            }

            if ($request->from)
            {
                $q->whereDate('created_at' , '>=' , Helper::convertDateTime($request->from));
            }

            if ($request->to)
            {
                $q->whereDate('created_at' , '<=' , Helper::convertDateTime($request->to));
            }


        })->latest()->paginate(30);

        return $this->view('index' , compact('records','folder','folders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store($id , Request $request)
    {
        $user = auth()->user();

        $rules =
            [
                'name' => 'required|unique:files,name',
                'status' => 'required|in:public,private',
            ];

        $message =
            [
                'name.required' => 'الرجاء ادخال الاسم',
                'status.required' => 'الرجاء إختيار حالة الملف',
                'name.unique' => 'يوجد ملف بهذا الإسم بالفعل',

            ];

        $data = validator()->make($request->all(),$rules , $message);

        if($data->fails())
            return $this->returnError($data);

        $folder = $this->folderPermission()->findOrFail($id);

        $request->merge(['user_id' => $user->id]);

        $record = $folder->files()->create($request->all());

        session()->flash('success', 'تمت الاضافة بنجاح');
        $this->viewsUrl = 'folder/'.$folder->id.'/files';
        return $this->returnSuccess();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * @param $folder
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($folder , Request $request, $id)
    {
        $user = auth()->user();
        $folder = $this->folderPermission()->findOrFail($folder);
        $record = $this->filePermission($folder)->where('user_id' , $user->id)->findOrFail($id);

        $rules =
            [
                'edit_name' => 'required|unique:files,name,'.$id,
                'edit_status' => 'required|in:public,private',
            ];

        $message =
            [
                'edit_name.required' => 'الرجاء ادخال الاسم',
                'edit_status.required' => 'الرجاء إختيار حالة الملف',
                'edit_name.unique' => 'يوجد ملف بهذا الإسم بالفعل',

            ];
        $data = validator()->make($request->all(),$rules,$message);

        if($data->fails())
        {
            session()->flash('fail', 'الرجاء ادخال الاسم');
            return back();
        }

        $request->merge([
            'name' => $request->edit_name,
            'status' => $request->edit_status,
        ]);

        $record->update($request->all());

        session()->flash('success', 'تمت التعديل بنجاح');
        return back();
    }

    /**
     * @param $folder
     * @param $id
     * @return mixed
     */
    public function destroy($folder , $id)
    {
        $user = auth()->user();
        $folder = $this->folderPermission()->findOrFail($folder);
        $record = $this->filePermission($folder)->where('user_id' , $user->id)->findOrFail($id);

        if($record->contents()->count()) {
            $data = [
                'status' => 0,
                'msg' => ' فشل الحذف المجلد يحتوي علي بعض الملفات',
                'id' => $id
            ];
            return Response::json($data, 200);
        }

        $record->delete();

        $data = [
            'status' => 1,
            'msg' => 'تم الحذف بنجاح',
            'id' => $id
        ];
        return Response::json($data, 200);
    }
}