<?php
namespace Admin;

use App\Charts\GeneralChart;
use App\Http\Controllers\Controller;
use App\Models\File;
use App\Models\Folder;
use App\Traits\Query;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    use Query;

    public function index(Request $request)
    {
//        $pies = $this->buildPieChart($request);
//        $pies_folders = $pies['folder_chart'];
//        $pies_files = $pies['file_chart'];
        return view('layouts.home');
    }

    protected function buildPieChart(Request $request)
    {

        $private_folders = Folder::where('status' , 'private')->count();

        $public_folders = Folder::where('status' , 'public')->count();

        $private_files = File::where('status' , 'private')->count();

        $public_files = File::where('status' , 'public')->count();

        $folderPieChart = new GeneralChart;
        $filePieChart = new GeneralChart;

        $folderPieChart
            ->labels([ 'المجلدات الخاصة', 'المجلدات العامة'])
            ->dataset("المجلدات",'pie',[$private_folders,$public_folders])
            ->backgroundcolor(["rgb(76, 175, 80)","rgb(175, 20, 20)"]);

        $filePieChart
            ->labels([ 'الملفات الخاصة', 'الملفات العامة'])
            ->dataset("الملفات",'pie',[$private_files,$public_files])
            ->backgroundcolor(["rgb(76, 175, 80)","rgb(175, 20, 20)"]);

        return [
            'folder_chart' => $folderPieChart,
            'file_chart' => $filePieChart
        ];
    }
}
