<?php
namespace Admin;

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\Master;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    use Master;


    public function __construct()
    {
        $this->model = new User();
        $this->viewsDomain = 'reset-password.';
        $this->viewsUrl = 'reset-password';
    }

    public function viewLogin()
    {
        return view('auth.login');
    }


    public function login(Request $request)
    {
        $rules = [
            'email' => 'required|email|exists:users,email',
            'password' => 'required'
        ];

        $message = [
            'email.required' => 'البريد الإلكترني مطلوب',
            'email.email' => 'الرجاء ادخال البريد الإلكتروني بشكل صحيح',
            'email.exists' => 'الرجاء ادخال البريد الإلكتروني بشكل صحيح',
            'password.required' => 'كلمة المرور مطلوبة'
        ];


        $data = validator()->make($request->all(), $rules , $message);

        if ($data->fails()) {
            return back()->withInput()->withErrors($data->errors());

        }else{

            $remember = $request->input('remember') && $request->remember == 1 ? $request->remember : 0;

            if(auth()->guard('admin')->attempt(['email' => $request->email , 'password' => $request->password],$remember))
            {
                return back();
            }else{

                return back()->withInput()->withErrors(['email' => 'خطأ في البريد الإلكتروني أو كلمة المرور']);
            }
        }

    }

    //reset password


    public function ResetPasswordView()
    {
        return view('reset_password.index');
    }

    public function ResetPassword(Request $request)
    {
        $rules =
            [

                'old_password' => 'required',
                'password' => 'required|confirmed',

            ];

        $error_sms =
            [
                'old_password.required'=>'الرجاء ادخال كلمة المرور الحالية ',
                'password.required'=>'الرجاء ادخال كلمة المرور الجديدة',
                'password.confirmed'=>'الرجاء التاكد من كلمة المرور الجديدة',

            ];

        $validator = validator()->make($request->all() , $rules , $error_sms);

        if($validator->fails())
            return $this->returnError($validator);

        if(Hash::check($request->old_password , auth('admin')->user()->password))
        {
            auth('admin')->user()->update(['password'=>Hash::make($request->password)]);


            session()->flash('success' , 'تمت تحديث بنجاح');
            return $this->returnSuccess();

        }else{

                return response()->json([

                    'status' => 0,
                    'error' => true,
                    'errors' => ['old_password' => 'الرجاء ادخال كلمة المرور الحالية '],
                    'code' => 400
                ]);
        }

    }



    public function logout(Request $request)
    {
        return (new LoginController())->logout($request);
    }
}
