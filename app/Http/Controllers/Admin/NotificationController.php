<?php

namespace Admin;

use App\Models\Folder;
use App\Traits\Master;
use Illuminate\Support\Facades\Artisan;
use Spatie\Permission\Models\Permission;
use \Spatie\Permission\Models\Role;
use Helper\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;

class NotificationController extends Controller
{
    use Master;

    public function __construct()
    {
        $this->model = new Folder();
        $this->viewsDomain = 'notifications.';
        $this->viewsUrl = 'notifications';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = auth()->user();
        $records = $user->notifications()->where(function ($q) use ($request) {

            if ($request->name) {
                $q->where(function ($q) use ($request) {

                    $q->where('name', 'LIKE', '%' . $request->name . '%');
                });
            }

            if ($request->from) {
                $q->whereDate('created_at', '>=', Helper::convertDateTime($request->from));
            }

            if ($request->to) {
                $q->whereDate('created_at', '<=', Helper::convertDateTime($request->to));
            }


        })->latest()->paginate(30);

        return $this->view('index', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =
            [
                'name' => 'required',

            ];

        $message =
            [
                'name.required' => 'الرجاء ادخال الاسم',

            ];

        $data = validator()->make($request->all(), $rules, $message);

        if ($data->fails())
            return $this->returnError($data);


        $record = $this->model->create($request->all());

        session()->flash('success', 'تمت الاضافة بنجاح');
        return $this->returnSuccess();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $record = $this->model->findOrFail($id);

        $rules =
            [
                'edit_name' => 'required',

            ];

        $message =
            [
                'edit_name.required' => 'الرجاء ادخال الاسم',

            ];
        $data = validator()->make($request->all(), $rules, $message);

        if ($data->fails()) {
            session()->flash('fail', 'الرجاء ادخال الاسم');
            return back();
        }

        $record->update(['name' => $request->edit_name]);

        session()->flash('success', 'تمت التعديل بنجاح');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $record = $this->model->findOrFail($id);

        if ($record->files()->count()) {
            $data = [
                'status' => 0,
                'msg' => ' فشل الحذف المجلد يحتوي علي بعض الملفات',
                'id' => $id
            ];
            return Response::json($data, 200);
        }

        $record->delete();

        $data = [
            'status' => 1,
            'msg' => 'تم الحذف بنجاح',
            'id' => $id
        ];
        return Response::json($data, 200);
    }


    public function read($id)
    {
        $user = auth()->user();
        $record = $user->notifications()->findOrFail($id);

        $record->users()->updateExistingPivot($user->id, ['is_read' => 1]);

        $data = [
            'status' => 1,
            'msg' => '',
            'id' => $id
        ];
        return Response::json($data, 200);
    }
}