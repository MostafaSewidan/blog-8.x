<?php

namespace Admin;

use App\Http\Controllers\Controller;
use Helper\Helper;
use App\Traits\Master;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Models\User;
use Response;
use Hash;
use Auth;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    use Master;

    public function __construct()
    {
        $this->model = new User();
        $this->viewsDomain = 'users.';
        $this->viewsUrl = 'users';
    }

    public function index(Request $request)
    {
        $users = User::where(function ($q) use ($request) {

            if($request->id){
                $q->where('id' , $request->id);
            }else{

                if ($request->name) {
                    $q->where(function ($q) use ($request) {

                        $q->where('name', 'LIKE', '%' . $request->name . '%')
                            ->orWhere('email', 'LIKE', '%' . $request->name . '%');
                    });
                }
                if ($request->role_name) {

                    $q->whereHas('roles', function ($q) use ($request) {

                        $q->where('display_name', 'LIKE', '%' . $request->role_name . '%');
                    });
                }

                if ($request->from) {
                    $q->whereDate('created_at', '>=', Helper::convertDateTime($request->from));
                }

                if ($request->to) {
                    $q->whereDate('created_at', '<=', Helper::convertDateTime($request->to));
                }

            }

        })->latest()->paginate(20);

        return $this->view('index', compact('users'));
    }

    public function create(User $model)
    {
        $model = new User();
        $roles = Role::all();

        return $this->view('create', compact('model', 'roles'));
    }

    public function store(Request $request)
    {
        $rules =
            [
                'name'     => 'required',
                'email'    => 'required|email|unique:users',
                'password' => 'required|confirmed',
                'roles.*'  => 'required|exists:roles,id',
            ];

        $error_sms =
            [
                'name.required'      => 'الرجاء ادخال الاسم ',
                'email.unique'       => ' البريد الالكتروني موجود بالفعل',
                'email.required'     => 'الرجاء ادخال البريد الالكتروني',
                'password.required'  => 'الرجاء ادخال كلمة المرور',
                'password.confirmed' => 'الرجاء التاكد من كلمة المرور ',

            ];

        $data = validator()->make($request->all(), $rules, $error_sms);

        if ($data->fails()) {
            return back()->withInput()->withErrors($data->errors());
        }

        DB::beginTransaction();
        $user = User::create(request()->all());

        $user->update(['password' => Hash::make($request->password)]);

        $user->assignRole($request->roles);
        DB::commit();
        session()->flash('success', 'تمت الاضافة بنجاح');
        return redirect('/users');
    }

    public function edit($id)
    {
        if ($id == 1)
            return redirect('users');

        $model = User::findOrFail($id);
        $roles = Role::all();

        return $this->view('edit', compact('model', 'roles'));
    }

    public function update(Request $request, $id)
    {
        if ($id == 1)
            return back();
        $record = User::findOrFail($id);

        $rules =
            [
                'name'     => 'required',
                'email'    => 'required|email|unique:users,email,' . $record->id . '',
                'password' => 'nullable|confirmed',
                'roles.*'  => 'required|exists:roles,id',
            ];

        $error_sms =
            [
                'name.required'      => 'الرجاء ادخال الاسم ',
                'email.required'     => 'الرجاء ادخال البريد الالكتروني',
                'email.unique'       => ' البريد الالكتروني موجود بالفعل',
                'password.required'  => 'الرجاء ادخال كلمة المرور ',
                'password.confirmed' => 'الرجاء التاكد من كلمة المرور ',
            ];

        $data = validator()->make($request->all(), $rules, $error_sms);

        if ($data->fails()) {
            return redirect('/users/' . $id . '/edit')->withInput()->withErrors($data->errors());
        }


        $record->update($request->except('password'));

        if ($request->has('password')) {
            $record->update(['password' => Hash::make($request->password)]);
        }

        DB::table('model_has_roles')->where('model_id', $id)->delete();
        $record->assignRole($request->roles);

        session()->flash('success', 'تم التعديل بنجاح');
        return back();

    }

    public function destroy($id)
    {
        $record = User::find($id);

        if (!$record) {
            return response()->json([
                'status' => 0,
                'message' => 'تعذر الحصول على البيانات'
            ]);
        }

        if (auth()->user()->id == $record->id) {
            session()->flash('fail', 'This email, you cannot deactivate it');
            return redirect('users');
        }

        DB::table('model_has_roles')->where('model_id', $id)->delete();
        $record->delete();
        $data = [
            'status' => 1,
            'msg'    => 'تم الحذ بنجاح',
            'id'     => $id
        ];

        return Response::json($data, 200);
    }


    public function toggleBoolean($id, $action)
    {
        $record = User::findOrFail($id);

        if (auth('admin')->user()->id == $record->id) {

            return Helper::responseJson(0, 'لا يمكنك إلغاء تفعيل حسابك ');
        }

        $activate = Helper::toggleBoolean($record, $action, 1, 0);

        if ($activate) {
            return Helper::responseJson(1, 'تمت العملية بنجاح');
        }

        return Helper::responseJson(0, 'حدث خطأ');
    }

    public function home()
    {
        return view('layouts.home');
    }
}
