<?php

namespace Admin;

use App\Http\Controllers\Controller;
use App\Models\Log;
use App\Traits\Master;
use Illuminate\Http\Request;

class LogController extends Controller
{
    use Master;

    public function __construct()
    {
        $this->model = new Log();
        $this->viewsDomain = 'logs.';
        $this->viewsUrl = 'logs';
    }

    /**
     * @param $view
     * @param array $params
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $records = Log::where(function ($q) use ($request){

            if ($request->type) {
                $q->where('logable_type', 'App\Models\\' . ucfirst($request->type));
            }

            if ($request->id) {
                $q->where('logable_id',  $request->id);
            }

            if ($request->user_id) {
                $q->where('user_id',  $request->user_id);
            }

            if ($request->from) {
                $q->whereDate('created_at', '>=', $request->from);
            }

            if ( $request->to) {

                $q->whereDate('created_at', '<=', $request->to);
            }

        })->latest()->paginate($request->input('paginate' , 15));

        return $this->view('index', compact('records'));
    }


}
