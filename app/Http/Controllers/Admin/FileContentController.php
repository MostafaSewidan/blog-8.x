<?php

namespace Admin;

use App\Models\Content;
use App\Models\File;
use App\Models\Log;
use App\Traits\Master;
use Helper\Attachment;
use Helper\NotificationHelper;
use Illuminate\Support\Facades\Artisan;
use Spatie\Permission\Models\Permission;
use \Spatie\Permission\Models\Role;
use Helper\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;

class FileContentController extends Controller
{
    use Master;

    public function __construct()
    {
        $this->model = new Content();
        $this->viewsDomain = 'contents.';
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($id , Request $request)
    {
        $file = File::findOrFail($id);

        $records = $file->contents()->where(function($q) use($request){

            if (auth()->user()->can('show_his_contents') && !auth()->user()->can('show_all_contents')) {

                    $q->whereHas('users', function ($q) use ($request) {
                        $q->where('user_id' , auth()->user()->id);
                    });
            }

            if($request->id)
            {
                $q->where('id' , $request->id);
            }else{
                if($request->name)
                {
                    $q->where(function ($q) use($request){

                        $q->where('title','LIKE','%'.$request->name.'%')
                            ->orWhere('description','LIKE','%'.$request->name.'%');
                    });
                }

                if ($request->from)
                {
                    $q->whereDate('created_at' , '>=' , Helper::convertDateTime($request->from));
                }

                if ($request->to)
                {
                    $q->whereDate('created_at' , '<=' , Helper::convertDateTime($request->to));
                }
            }

        })->latest()->paginate(30);

        return $this->view('index' , compact('records','file'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($id)
    {
        $record = $this->model;
        $file = File::findOrFail($id);
        return $this->view('create',compact('file','record'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store($id , Request $request)
    {
        $rules=
            [
                'users'=>'nullable|exists:users,id',
                'title'=>'required|max:200',
                'description'=>'required',
                'is_access'=>'nullable|in:0,1',
                'attachments'=>'nullable|array',
                'attachments.*'=>'max:10000',
            ];

        $message=
            [
                'title.required'=>' ',
                'description.required'=>' ',
                'attachments.*'=>'حجم الملفات كبير جدا',
            ];

        $data = validator()->make($request->all(),$rules , $message);

        if($data->fails())
            return $this->returnError($data);

        $user = auth()->user();

        $file = File::findOrFail($id);

        $record = $file->contents()->create($request->all());

        if ($request->users)
        {
           $record->users()->attach($request->users);
           NotificationHelper::sendNotification(
            $record,
            $request->users,
            'users',
            'تم مشاركة مقال معك',
            'قام ' .
            $user->name .
            ' بمشاركة المقال #' .
            $record->id.'معك');

        }

        if ($request->attachments && count($request->attachments))
        {
            foreach ($request->attachments as $attachment)
            {
                Attachment::addAttachment($attachment,$record,'contents');
            }
        }

        Log::createLog($record , $user ,
        'قام '.
            $user->name.
            ' بإضافة المقال رقم #'.
            $record->id
            , null ,'add'
        );
        session()->flash('success', 'تمت الاضافة بنجاح');
        $this->viewsUrl = 'file/'.$file->id.'/contents';
        return $this->returnSuccess();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param $file
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($file , $id)
    {
        $file = File::findOrFail($file);
        $record = $file->contents()->findOrFail($id);
        return $this->view('edit',compact('file','record'));
    }

    /**
     * @param $file
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($file , Request $request, $id)
    {
        $file = File::findOrFail($file);
        $record = $file->contents()->findOrFail($id);
        $user = auth()->user();

        $rules=
            [
                'users'=>'nullable|exists:users,id',
                'title'=>'required|max:200',
                'description'=>'required',
                'is_access'=>'nullable|in:0,1',
                'attachments'=>'nullable|array',
                'attachments.*'=>'max:10000',
            ];

        $message=
            [
                'title.required'=>' ',
                'description.required'=>' ',
                'attachments.*'=>'حجم الملفات كبير جدا',
            ];
        $data = validator()->make($request->all(),$rules,$message);

        if($data->fails())
            return $this->returnError($data);


        $record->update($request->all());

            $record->users()->sync($request->users);

        if ($request->attachments && count($request->attachments))
        {
            foreach ($request->attachments as $attachment)
            {
                Attachment::addAttachment($attachment,$record,'contents');
            }
        }

        Log::createLog($record , $user ,
            'قام '.
            $user->name.
            ' بتعديل المقال رقم #'.
            $record->id
        );
        session()->flash('success', 'تمت التعديل بنجاح');
        $this->viewsUrl = 'file/'.$file->id.'/contents/'.$record->id.'/edit';
        return $this->returnSuccess();
    }

    /**
     * @param $file
     * @param $id
     * @return mixed
     */
    public function destroy($file , $id)
    {
        $file = File::findOrFail($file);
        $record = $file->contents()->findOrFail($id);
        $user = auth()->user();

        if(!$record) {
            $data = [
                'status' => 0,
                'msg' => ' تعذر الحصول علي البيانات',
                'id' => $id
            ];
            return Response::json($data, 200);
        }

        if($record->attachmentRelation()->count())
            Attachment::deleteAttachment($record,['multi' => true]);

        $record->users()->detach();
        $record->delete();
        Log::createLog($record , $user ,
            'قام '.
            $user->name.
            ' بحذف المقال رقم #'.
            $record->id , null , 'remove'
        );
        $data = [
            'status' => 1,
            'msg' => 'تم الحذف بنجاح',
            'id' => $id
        ];
        return Response::json($data, 200);
    }

    /**
     * @param $file
     * @param $content
     * @param $action
     * @return \Illuminate\Http\RedirectResponse
     */
    public function toggleBoolean($file , $content , $action)
    {
        $file = File::findOrFail($file);
        $record = $file->contents()->findOrFail($content);
        $user = auth()->user();
        $action_title = $record->$action == 0 ? 'من حالة قابل للتعديل إلي القرائة فقط' : 'من حالة القرائة فقط إلي قابل للتعديل';
        $activate = Helper::toggleBoolean($record, $action, 1, 0);

        if ($activate)
        {
            Log::createLog($record , $user ,
                'قام '.
                $user->name.
                ' بتغيير حالة المقال رقم #'.
                $record->id.
                $action_title
            );
            return Helper::responseJson(1, 'تمت العملية بنجاح');
        }

        return Helper::responseJson(0, 'حدث خطأ');
    }
}