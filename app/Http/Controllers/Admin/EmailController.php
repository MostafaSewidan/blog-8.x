<?php

namespace Admin;

use App\Mail\SendEmail;
use App\Models\Email;
use App\Models\User;
use App\Traits\Master;
use Helper\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Response;

class EmailController extends Controller
{
    use Master;

    public function __construct()
    {
        $this->model = new Email();
        $this->viewsDomain = 'emails.';
        $this->viewsUrl = 'emails';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $records = $this->model->where(function ($q) use ($request) {

            if ($request->name) {
                $q->where(function ($q) use ($request) {

                    $q->where('title', 'LIKE', '%' . $request->name . '%')
                    ->orWhere('content', 'LIKE', '%' . $request->name . '%');
                });
            }

            if ($request->from) {
                $q->whereDate('created_at', '>=', Helper::convertDateTime($request->from));
            }

            if ($request->to) {
                $q->whereDate('created_at', '<=', Helper::convertDateTime($request->to));
            }


        })->latest()->paginate(30)->appends(\Request::except('page'));

        return $this->view('index', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = $this->model;

        return $this->view('create',compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth('admin')->user();
        $rules =
            [
                'title' => 'required',
                'content' => 'nullable',
                'users' => 'required|array',
                'users.*' => 'exists:users,id',
            ];

        $message =
            [
                'title.required' => 'العنوان مطلوب',
                'users.required' => 'المستخدمين مطلوبين مطلوب',
                'users.*.exists' => 'المستخدمين مطلوبين مطلوب',

            ];

        $data = validator()->make($request->all(), $rules, $message);

        if ($data->fails())
            return $this->returnError($data);

        $record = $user->emails()->create($request->all());

        foreach ($request->users as $user_id)
        {
            $to = User::find($user_id);

            if($to)
                Mail::to($to->email)->send(new SendEmail($record));
        }

        session()->flash('success', 'تمت الإرسال بنجاح');
        return $this->returnSuccess();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->model->findOrFail($id);

        return $this->view('edit',compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = auth()->user();
        $record = $this->model->findOrFail($id);

        $rules =
            [
                'title' => 'required',
                'content' => 'nullable',
                'users' => 'required|array',
                'users.*' => 'exists:users,id',
            ];

        $message =
            [
                'title.required' => 'العنوان مطلوب',
                'users.required' => 'المستخدمين مطلوبين مطلوب',
                'users.*.exists' => 'المستخدمين مطلوبين مطلوب',

            ];
        $data = validator()->make($request->all(), $rules, $message);

        if ($data->fails())
            return $this->returnError($data);

        $record->update($request->all());

        session()->flash('success', 'تمت إعادة الإرسال بنجاح');
        return $this->returnSuccess();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = auth()->user();
        $record = $this->model->findOrFail($id);

        $record->delete();

        $data = [
            'status' => 1,
            'msg' => 'تم الحذف بنجاح',
            'id' => $id
        ];
        return Response::json($data, 200);
    }
}