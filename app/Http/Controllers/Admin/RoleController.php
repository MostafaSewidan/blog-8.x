<?php

namespace Admin;

use App\Traits\Master;
use Illuminate\Support\Facades\Artisan;
use Spatie\Permission\Models\Permission;
use \Spatie\Permission\Models\Role;
use Helper\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;

class RoleController extends Controller
{
    use Master;

    public function __construct()
    {
        $this->model = new Role();
        $this->viewsDomain = 'roles.';
        $this->viewsUrl = 'roles';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles = Role::where(function($q) use($request){
            if($request->name)
            {
                $q->where(function ($q) use($request){

                    $q->where('display_name','LIKE','%'.$request->name.'%');
                });
            }

            if ($request->from)
            {
                $q->whereDate('created_at' , '>=' , Helper::convertDateTime($request->from));
            }

            if ($request->to)
            {
                $q->whereDate('created_at' , '<=' , Helper::convertDateTime($request->to));
            }


        })->latest()->paginate(20);

        return $this->view('index' , compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        Artisan::call('permission:cache-reset');
        $model = new Role();
        $permissions = Permission::orderBy('order')->get();
        return $this->view('create',compact('model' , 'permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=
            [
                'name'=>'required|unique:roles',
                'display_name'=>'required',

            ];

        $message=
            [
                'name.required'=>'الرجاء ادخال الاسم',
                'name.unique'=>'تم اخال هذه الصلاحية من قبل',
                'display_name.required'=>'الرجاء اخال الاسم المعروض',

            ];

        $data = validator()->make($request->all(),$rules , $message);
        if($data->fails())
        {
            return back()->withInput()->withErrors($data->errors());
        }

        $record = Role::create(request()->all());

        if($request->permissions)
            $record->permissions()->attach($request->permissions);

        session()->flash('success', 'تمت الاضافة بنجاح');
        return redirect('roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if($id == 2)
            return redirect('roles');

        Artisan::call('permission:cache-reset');
        $model = Role::findOrFail($id);

        $permissions = Permission::orderBy('order')->get();

        return $this->view('edit', compact('model' , 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $record = Role::findOrFail($id);

        $rules=
            [
                'name'=>'required|unique:roles,name,'.$record->id.'',
                'display_name'=>'required',
                'permissions'=>'required',

            ];

        $message=
            [
                'name.required'=>'الرجاء ادخال الاسم',
                'name.unique'=>'تم اخال هذه الصلاحية من قبل',
                'display_name.required'=>'الرجاء اخال الاسم المعروض',

            ];
        $data = validator()->make($request->all(),$rules,$message);

        if($data->fails())
        {
            return back()->withInput()->withErrors($data->errors());
        }

        $record ->update($request->all());
        $record->permissions()->sync($request->permissions);

        session()->flash('success', 'تمت التعديل بنجاح');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        $record = Role::findOrFail($id);

        $users = $record->users()->get();

        if(!count($users) || $id != 2)
        {
            $record->permissions()->detach();
            $record->delete();

            $data = [
                'status' => 1,
                'msg' => 'تم الحذف بنجاح',
                'id' => $id
            ];
            return Response::json($data, 200);
        }else
        {
            $data = [
                'status' => 0,
                'msg' => ' فشل الحذف , يوجد مستخدمين مرتبطين بهذه الصلاحية',
                'id' => $id
            ];
            return Response::json($data, 200);
        }
    }
}